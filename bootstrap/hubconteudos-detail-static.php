
<?php include 'header.php'; ?>

<div class="container main">

	<!-- Advertisement -->
<div class="row hidden-print">
	<div class="col-xs-12">
		<figure class="add secondary" style="background-image:url('dist/images/hubBanner.jpg');">
			<figcaption>
				<div class="row row-xs-flex center">
					<div class="col-xs-12 col-sm-7 col-md-8 text-center">
						<h3>Bem-vindo ao hub de conteúdos Lusíadas</h3>
						<em class="hidden-sm hidden-xs">Encontre tudo o que sempre quis saber sobre a sua saúde e a de toda a família</em>
					</div>
					<div class="col-sm-5 col-md-4 more orange hidden-xs">
						<a href="" class="btn btn-orange">Subscrever E-News</a>
					</div>
				</div>
			</figcaption>
		</figure>	
	</div>
</div>

	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Lusiadas.pt</a></li>
				<li class="active">Hub de conteúdos</li>
				<li class="active">Título artigo</li>
			</ol>
		</div>
	</div>

	<div class="row articles">
		<div class="col-md-8 main-content">

			<!-- Header -->
			<div class="row">
				<div class="col-xs-12 page-header">
					<h1 class="page-title">Aenean eu leo quam. Pellentesque ornare</h1>
					<p class="lead">Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec ullamcorper nulla non metus auctor fringilla. Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
					<span class="author">Por Dra. Clara Pinto | 03-04-2014</span>
				</div>
			</div>

			<!-- Content -->
			
			<div class="row">
				<div class="col-xs-12">
					<img src="dist/images/slideshow4.jpg" class="img-responsive cover-image">
					<div class="default-caption">Vestibulum id ligula porta felis euismod semper.</div>
					<div class="article-share hide-xs">
						<a href="" class="font-up"></a>
						<a href="" class="font-down"></a>
						<a href="" class="print"></a>

						<a class="email trigger"></a>
						<div class="content hidden-xs hidden-sm">
							<form role="form" class="clearfix">
								<div class="form-group">
									<label for="exampleInputEmail1">Endereço de email</label>
									<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Insira endereço de email">
								</div>
								<a class="btn btn-secondary uppercase pull-right">Enviar</a>
							</form>
						</div>
						<!-- Social plugins  -->
						<span class="social">
							<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-action="like" data-show-faces="false" data-share="true"></div>
							<script src="//platform.linkedin.com/in.js" type="text/javascript">
							lang: en_US
							</script>
							<script type="IN/Share"></script>
							<a href="https://twitter.com/share" class="twitter-share-button" data-via="" data-count="none">Tweet</a>
						</span>

					</div>
					<p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Maecenas sed diam eget risus varius blandit sit amet non magna. Donec id elit non mi porta gravida at eget metus. Donec sed odio dui. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>

					<p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Maecenas sed diam eget risus varius blandit sit amet non magna. Donec id elit non mi porta gravida at eget metus. Donec sed odio dui. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>

					<p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Maecenas sed diam eget risus varius blandit sit amet non magna. Donec id elit non mi porta gravida at eget metus. Donec sed odio dui. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>

					<p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Maecenas sed diam eget risus varius blandit sit amet non magna. Donec id elit non mi porta gravida at eget metus. Donec sed odio dui. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>

					<p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Maecenas sed diam eget risus varius blandit sit amet non magna. Donec id elit non mi porta gravida at eget metus. Donec sed odio dui. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>

					<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Nullam quis risus eget urna mollis ornare vel eu leo. Etiam porta sem malesuada magna mollis euismod. Nullam quis risus eget urna mollis ornare vel eu leo. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae elit libero, a pharetra augue. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper.</p>

					<p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Maecenas sed diam eget risus varius blandit sit amet non magna. Donec id elit non mi porta gravida at eget metus. Donec sed odio dui. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>

					<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Nullam quis risus eget urna mollis ornare vel eu leo. Etiam porta sem malesuada magna mollis euismod. Nullam quis risus eget urna mollis ornare vel eu leo. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae elit libero, a pharetra augue. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper.</p>

					<p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Maecenas sed diam eget risus varius blandit sit amet non magna. Donec id elit non mi porta gravida at eget metus. Donec sed odio dui. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
				</div>
			</div>

			<!-- Comments -->
			<div class="row comments">
				<div class="col-md-12 row-xs-flex">
					<a class="btn btn-select btn-block uppercase pull-left" data-toggle="collapse" data-target="#addComment" id="mainButton">Deixe um comentário</a>
					<div class="pull-right actions text-center">
						<span>6</span>
						<span class="fa fa-comment"></span>
						<a data-toggle="collapse" data-target="#comments" class="arrow-toggle show-comment center collapsed">
							<span class=""></span>
						</a>
					</div>
				</div>
			</div>
			<div class="row comments-area">
				<div class="col-md-12">
					<div id="addComment" class="panel panel-default panel-collapse collapse success clearfix">
						<span class="title">Comentar esta notícia</span>
						<form role="form">
							<div class="form-group">
								<label for="nome">Nome</label>
								<input type="text" class="form-control">
							</div>
							<div class="form-group">
								<label for="comentario">Comentário</label>
								<p class="help-block pull-right">Caracteres disponíveis: 250</p>
								<textarea class="form-control"></textarea>
							</div>
							<div class="form-group pull-right">
								<a href="#mainButton" class="link" data-toggle="collapse" data-target="#addComment">Cancelar</a>
								<a href="#mainButton" class="btn btn-secondary uppercase" data-toggle="collapse" data-target=".success">Enviar comentário</a>
							</div>
						</form>
					</div>
					<div id="sentComment" class="panel success panel-collapse collapse panel-default clearfix">
						<span class="title">Comentário enviado!</span>
						<p class="lead">
							O Grupo Lusíadas agradece a sua participação!
						</p>
						<p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
						<div class="pull-right">
							<a href="#mainButton" class="btn btn-secondary uppercase" data-toggle="collapse" data-target="#sentComment">OK</a>
						</div>
					</div>
					<div id="comments" class="panel panel-default panel-collapse collapse">
						<span class="title">6 comentários</span>
						<div class="space">
							<span class="author">Pedro Gonçalves</span> <span class="date">11-03-2014, 12:45</span>
							<p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Cras mattis consectetur purus sit amet fermentum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Maecenas sed diam eget risus varius blandit sit amet non magna. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
						</div>
						<div class="space">
							<span class="author">Pedro Gonçalves</span> <span class="date">11-03-2014, 12:45</span>
							<p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Cras mattis consectetur purus sit amet fermentum.</p>
						</div>
						<div class="space">
							<span class="author">Pedro Gonçalves</span> <span class="date">11-03-2014, 12:45</span>
							<p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Cras mattis consectetur purus sit amet fermentum.</p>
						</div>
						<div class="space">
							<span class="author">Pedro Gonçalves</span> <span class="date">11-03-2014, 12:45</span>
							<p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Cras mattis consectetur purus sit amet fermentum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
						</div>
						<div class="space">
							<span class="author">Pedro Gonçalves</span> <span class="date">11-03-2014, 12:45</span>
							<p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Cras mattis consectetur purus sit amet fermentum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Maecenas sed diam eget risus varius blandit sit amet non magna. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
						</div>
						<div class="row">
							<div class="col-xs-12 text-center">
								<ul class="pagination">
									<li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
									<li><a href="#">2</a></li>
									<li class="arrow"><a href="#" class="next">Próxima página <span class="glyphicon glyphicon-arrow-right"></span></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Related articles -->
			<div class="row related">
				<div class="col-xs-12">
					<h3 class="area-title"><img src="dist/images/related.png">Artigos relacionados</h3>
				</div>
				<div class="col-sm-6 col-xs-12">
					<img src="dist/images/slideshow1.jpg" class="img-responsive">
					<span class="date">11-03-2014</span>
					<h4><a href="">Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</a></h4>
					<div class="ellipsis ellipsis-large">
						<div>
							<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
							<a href="hubconteudos-detail.php">Ler mais</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-xs-12">
					<div class="row">
						<div class="col-sm-6 col-xs-6 space">
							<img src="dist/images/slideshow1.jpg" class="img-responsive">
							<span class="date">11-03-2014</span>
							<div class="ellipsis">
								<div>
									<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
									<a href="hubconteudos-detail.php">Ler mais</a>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-xs-6">
							<img src="dist/images/slideshow1.jpg" class="img-responsive">
							<span class="date">11-03-2014</span>
							<div class="ellipsis">
								<div>
									<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
									<a href="hubconteudos-detail.php">Ler mais</a>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 col-xs-6">
							<img src="dist/images/slideshow1.jpg" class="img-responsive">
							<span class="date">11-03-2014</span>
							<div class="ellipsis">
								<div>
									<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
									<a href="hubconteudos-detail.php">Ler mais</a>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-xs-6">
							<img src="dist/images/slideshow1.jpg" class="img-responsive">
							<span class="date">11-03-2014</span>
							<div class="ellipsis">
								<div>
									<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
									<a href="hubconteudos-detail.php">Ler mais</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 sidebar hidden-print">
			<!-- Side nav -->
			<ul class="nav nav-pills nav-stacked side-nav">
				<li><a href="">Todas as categorias</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li class="active"><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li class="load-more"><a href="" class="uppercase">Ver mais <span class="glyphicon glyphicon-arrow-down"></span></a></li>
			</ul>
			
			<div class="panel">
				<img src="dist/images/hppEncontre.png" class="img-responsive">
			</div>
			<div class="panel">
				<img src="dist/images/hppMarcacoesSidebar.png" class="img-responsive">
			</div>

			<!-- Top posts -->
			<div class="panel panel-default top-posts">
				<div class="panel-heading">
					<h3 class="panel-title"><img src="dist/images/mais-vistos.png"> + Vistos</h3>
				</div>
				<div class="panel-body">
					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div>
									<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
									<a href="hubconteudos-detail.php">Ler mais</a>
								</div>
							</div>
						</div>
					</div>
					
					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div>
									<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
									<a href="hubconteudos-detail.php">Ler mais</a>
								</div>
							</div>
						</div>
					</div>

					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div>
									<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
									<a href="hubconteudos-detail.php">Ler mais</a>
								</div>
							</div>
						</div>
					</div>

					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div>
									<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
									<a href="hubconteudos-detail.php">Ler mais</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<a href="" class="btn btn-block btn-fade-1 uppercase">Hospitais e Clínicas</a>
			<a href="" class="btn btn-block btn-fade-2 uppercase">Médicos</a>
			<a href="" class="btn btn-block btn-fade-3 uppercase">Especialidades</a>
			<a href="" class="btn btn-block btn-fade-4 uppercase">Contactos</a>
			<a href="" class="btn btn-block btn-fade-5 uppercase">Fale connosco</a>
		</div>
	</div>

</div>


<?php include 'footer.php'; ?>