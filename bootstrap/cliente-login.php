
<?php include 'header-login.php'; ?>

<div class="container main">


	<div class="row login">

		<div class="col-md-4 col-md-push-8">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title uppercase">Login</h3>
					<strong>Clientes</strong>
				</div>
				<div class="panel-body">
					<form role="form">
						<div class="form-group">
							<label for="idcard">BI ou Cartão de Cidadão</label>
							<input type="text" class="form-control" id="idcard">
						</div>
						<div class="form-group">
							<label for="mobile">Telemóvel</label>
							<input type="text" class="form-control" id="mobile">
						</div>
						<div class="form-group">
							<label for="recaptcha">Validação de segurança</label>
						</div>

						<!-- Captcha -->

						<div class="form-group" id="recaptcha_widget">
							<div id="recaptcha_image"><img src="dist/images/image.jpeg"></div>
							<!-- <div class="recaptcha_only_if_incorrect_sol" style="color:red">Incorrect please try again</div> -->

							<p class="recaptcha_only_if_image">Insira aqui os caracteres que vê na imagem</p>
							<!-- <p class="recaptcha_only_if_audio">Insira aqui os números que ouve</p> -->

							<input type="text" id="recaptcha_response_field" name="recaptcha_response_field" class="form-control" />

							<p>Não consegue ler esta palavra? Tente com <a href="javascript:Recaptcha.reload()">outra palavra</a> ou com um
							<span class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type('audio')">audio CAPTCHA</a></span></p> 

							<!-- <div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type('image')">Get an image CAPTCHA</a></div> -->
							<!-- <div><a href="javascript:Recaptcha.showhelp()">Help</a></div> -->
							<small class="thin">Powered by reCAPTCHA</small>
						</div>


						
						<!-- End captcha -->

						<div class="form-group">
							<a href="cliente-homepage.php" class="btn btn-secondary uppercase btn-block btn-lg">Entrar</a>
						</div>
					</form>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-body">
					<strong>Ainda não é cliente?</strong>
					<h4 class="uppercase">Registe-se aqui</h4>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-body">
					<strong>Precisa de ajuda?</strong>
					<h4 class="uppercase">Contacte-nos</h4>
				</div>
			</div>
		</div>

		<div class="col-md-8 main-content col-md-pull-4">
			<h1 class="page-title">Portal Cliente Lusíadas</h1>
			<p class="lead">Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Cras mattis consectetur purus sit amet fermentum.</p>
			<p class="lead primary">Donec id elit non mi porta gravida at eget metus. Curabitur blandit tempus porttitor. Aenean lacinia bibendum nulla sed consectetur.</p>
			<div class="panel text-center">
				<img src="dist/images/medicos.png" class="img-responsive">
			</div>
		</div>

		
	</div>
</div>


<?php include 'footer-login.php'; ?>