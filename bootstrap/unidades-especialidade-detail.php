
<?php include 'header-unidades.php'; ?>

<div class="container main">
	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Hospital Lusíadas Lisboa</a></li>
				<li class="active"><a href="#">Especialidades</a></li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<h1 class="page-title">Especialidades</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-md-8 page-header">
			<p class="lead">Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec ullamcorper nulla non metus auctor fringilla. Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4 sidebar">
			<!-- Especialidades -->
			<label>Especialidades</label>
			<div class="panel fixed" id="especialidades">
				<ul class="list-unstyled">
					<li class="active"><a href="unidades-especialidade-detail.php" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
				</ul>
			</div>
		</div>

		<div class="col-md-8 main-content">
			<!-- Accordion -->
			<div class="panel-group accordion" id="acordos">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title blue">
							Cardiologia
						</h3>
					</div>

					<div class="panel-body">
						<!-- Steps -->
						<ul class="nav nav-tabs steps" id="servicos-tab">
							<li class="active"><a href="#descricao" data-toggle="tab">Descrição</a></li>
							<li><a href="#medicos" data-toggle="tab">Médicos</a></li>
							<li><a href="#areas" data-toggle="tab">Áreas de intervenção</a></li>
							<li><a href="#exames" data-toggle="tab">Exames</a></li>
							<li><a href="#marcacoes" data-toggle="tab">Marcações</a></li>
						</ul>
						<div class="tab-content" id="servicos-tabContent">
							<div class="tab-pane active" id="descricao">
								<p>Nulla vitae elit libero, a pharetra augue. Donec ullamcorper nulla non metus auctor fringilla. Nullam quis risus eget urna mollis ornare vel eu leo. Donec id elit non mi porta gravida at eget metus.</p>
								<img src="dist/images/slideshow1.jpg" class="img-responsive">
								<div class="default-caption">Vestibulum id ligula porta felis euismod semper.</div>
								<p>Nulla vitae elit libero, a pharetra augue. Donec ullamcorper nulla non metus auctor fringilla. Nullam quis risus eget urna mollis ornare vel eu leo. Donec id elit non mi porta gravida at eget metus.</p>
								<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Etiam porta sem malesuada magna mollis euismod. Maecenas sed diam eget risus varius blandit sit amet non magna. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Curabitur blandit tempus porttitor. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ullamcorper nulla non metus auctor fringilla. Maecenas sed diam eget risus varius blandit sit amet non magna. Curabitur blandit tempus porttitor.</p>

							</div>
							<div class="tab-pane list-medico" id="medicos">
								<div class="panel panel-default">
									<div class="panel-body white">
										<div class="media">
											<div class="col-xs-12 col-sm-3 col-md-6">
												<a href="#">
													<img class="img-responsive" src="dist/images/medico-thumb.jpg" alt="...">
												</a>
											</div>
											<div class="media-body col-xs-12 col-sm-9 col-md-6">
												<h3><a href="unidades-medico-detail.php">Dra. Ana Pedrosa</a></h3>
												<p>Coordenadora da Unidade de Anestesiologia</p>
												<p class="title">Principais áreas de interesse:</p>
												<div class="ellipsis">
													<div>
														<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
														<a href="hubconteudos-detail.php">Ler mais</a>
													</div>
												</div>
											</div>
										</div>
										<hr>
									</div>

										<div class="panel-body white">
											<div class="media ">
												<div class="col-xs-12 col-sm-3 col-md-6">
													<a href="#">
														<img class="img-responsive" src="dist/images/medico-thumb-default.jpg" alt="...">
													</a>
												</div>
												<div class="media-body col-xs-12 col-sm-9 col-md-6">
													<h3><a href="unidades-medico-detail.php">Dra. Ana Pedrosa</a></h3>
													<p>Coordenadora da Unidade de Anestesiologia</p>
													<p class="title">Principais áreas de interesse:</p>
													<div class="ellipsis">
														<div>
															<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
															<a href="hubconteudos-detail.php">Ler mais</a>
														</div>
													</div>
												</div>
											</div>
												<hr>
											</div>

											<div class="panel-body white">
												<div class="media ">
													<div class="col-xs-12 col-sm-3 col-md-6">
														<a href="#">
															<img class="img-responsive" src="dist/images/medico-thumb-default.jpg" alt="...">
														</a>
													</div>
													<div class="media-body col-xs-12 col-sm-9 col-md-6">
														<h3><a href="unidades-medico-detail.php">Dra. Ana Pedrosa</a></h3>
														<p>Coordenadora da Unidade de Anestesiologia</p>
														<p class="title">Principais áreas de interesse:</p>
														<div class="ellipsis">
															<div>
																<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
																<a href="hubconteudos-detail.php">Ler mais</a>
															</div>
														</div>
													</div>
												</div>
													<hr>
												</div>
											</div>
										</div>
										<div class="tab-pane" id="areas">

											<!-- Accordion -->
											<div class="panel-group" id="accordion">
												<div class="panel panel-default">
													<div class="panel-heading active">
														<h4 class="panel-title">
															<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="arrow-toggle">
																Acidente Vascular Cerebral <span class="pull-right"></span>
															</a>
														</h4>
													</div>
													<div id="collapseOne" class="panel-collapse collapse in">
														<div class="panel-body">

															<h5>Sintomas</h5>
															<p>Nulla vitae elit libero, a pharetra augue. Donec ullamcorper nulla non metus auctor fringilla. Nullam quis risus eget urna mollis ornare vel eu leo. Donec id elit non mi porta gravida at eget metus.</p>
															<h5>Tratamento</h5>
															<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Etiam porta sem malesuada magna mollis euismod. Maecenas sed diam eget risus varius blandit sit amet non magna. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Curabitur blandit tempus porttitor. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ullamcorper nulla non metus auctor fringilla. Maecenas sed diam eget risus varius blandit sit amet non magna. Curabitur blandit tempus porttitor.</p>
														</div>
													</div>
												</div>
												<div class="panel panel-default">
													<div class="panel-heading">
														<h4 class="panel-title">
															<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="arrow-toggle collapsed">
																Ullamcorper Amet Vulputate <span class="pull-right"></span>
															</a>
														</h4>
													</div>
													<div id="collapseTwo" class="panel-collapse collapse">
														<div class="panel-body">
															<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
														</div>
													</div>
												</div>
												<div class="panel panel-default">
													<div class="panel-heading">
														<h4 class="panel-title">
															<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="arrow-toggle collapsed">
																Ullamcorper Amet Vulputate <span class="pull-right"></span>
															</a> 
														</h4>
													</div>
													<div id="collapseThree" class="panel-collapse collapse">
														<div class="panel-body">
															<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="tab-pane" id="exames">
											Consulte <a href="unidades-exames.php">aqui</a> Aenean lacinia bibendum nulla sed consectetur.
										</div>
										<div class="tab-pane contact" id="marcacoes">
											<div class="media">
												<a class="pull-left" href="#">
													<img class="media-object" src="dist/images/telefone.png" alt="...">
												</a>
												<div class="media-body">
													<span class="blue">Nº marcação único</span>
													<span class="number terciary">800 20 1000</span>
													<p>Grátis</p>
													<p>24h todos os dias</p>
												</div>
											</div>

											<div class="media">
												<a class="pull-left" href="#">
													<img class="media-object" src="dist/images/mail.png" alt="...">
												</a>
												<div class="media-body">
													<span class="blue">Email</span>
													<span class="number terciary">geral@lusiadas.pt</span>
													<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Maecenas faucibus mollis interdum.</p>
												</div>
											</div>

											<div class="media">
												<a class="pull-left" href="#">
													<img class="media-object" src="dist/images/lusiadas-contactos.jpg" alt="...">
												</a>
												<div class="media-body clearfix">
													<div class="col-xs-12 col-sm-8">
														<span class="blue">Online</span>
														<span class="number terciary">Portal Cliente Lusíadas</span>
														<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>
													</div>
													<div class="col-xs-12 col-sm-4 app">
														<a href=""><img src="dist/images/marcar-contactos.jpg" class="img-responsive"></a>
													</div>
												</div>
											</div>

											<div class="media">
												<a class="pull-left" href="#">
													<img class="media-object" src="dist/images/person.png" alt="...">
												</a>
												<div class="media-body">
													<span class="blue">Presencialmente nas nossas unidades</span>
													<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Maecenas faucibus mollis interdum.</p>
												</div>
											</div>
										</div>
									</div> <!-- End tab content -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


<?php include 'footer.php'; ?>