$(function(){
    $('.selectpicker').selectpicker({
    	style: 'btn-select'
    });
});

$(function(){
    $('#medico-especialidades').slimScroll({
        height: '390px',
        size: '10px',
        railVisible: true,
    	alwaysVisible: true,
    	color: '#a2c2d8',
	    railColor: '#edf3f8',
	    railOpacity: 1
    });
});

$(function(){
    $('#especialidades').slimScroll({
        height: '490px',
        size: '10px',
        railVisible: true,
        alwaysVisible: true,
        color: '#a2c2d8',
        railColor: '#edf3f8',
        railOpacity: 1
    });
});


$(document).ready( function() {
    $('.newsTabs').carousel({
        interval:   4000
    });
    
    var clickEvent = false;
    $('.newsTabs').on('click', '.nav a', function() {
            clickEvent = true;
            $('.nav-pills li').removeClass('active');
            $(this).parent().addClass('active');        
    }).on('slid.bs.carousel', function(e) {
        if(!clickEvent) {
            var count = $('.nav-pills').children().length -1;
            var current = $('.nav-pills li.active');
            current.removeClass('active').next().addClass('active');
            var id = parseInt(current.data('slide-to'));
            if(count == id) {
                $('.nav-pills li').first().addClass('active');    
            }
        }
        clickEvent = false;
    });
     $('.newsTabs').on('click', '.nav a', function () {
        clickEvent = true;
        $('.nav li').removeClass('active');
        $(this).parent().addClass('active');
    });

});

$(function(){
    $('.trigger').popover({
        html : true,
        content: function() {
          return $(this).parent().find('.content').html();
        },
        container: '#s4-bodyContainer',
        placement: 'bottom'
    });
});

$(function(){
    $('.pop').popover({
        html : true,
        trigger: 'click focus',
        content: function() {
          return $(this).parent().find('.content').html();
        },
        container: '#s4-bodyContainer',
        placement: 'right'
    });
    $('.pop').click(function(){
        $('.pop').toggleClass('active');
    });
});

$(function(){
    $('.cal-trigger').popover({
        html : true,
        trigger: 'focus hover',
        content: function() {
          return $(this).parent().find('.content').html();
        },
        container: '#s4-bodyContainer',
        placement: 'right'
    });
    $('.cal-trigger').click(function(){
        $('#myModal').modal();
    });
});

$('.file-inputs').bootstrapFileInput();

$('.accordion').on('shown.bs.collapse', function () {
    var element = $(this).find('.in');
    element.parent().find('.panel-heading').addClass('active');
});

$('.accordion').on('hide.bs.collapse', function () {
    var element = $(this).find('.in');
    element.parent().find('.panel-heading').removeClass('active');
});




