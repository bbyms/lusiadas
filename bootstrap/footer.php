<footer class="container">
	<div class="row">
		<div class="col-xs-12">
			<strong class="space uppercase">Mapa do site</strong>
		</div>
	</div>

	<!-- Footer links -->
	<div class="row">
		<div class="col-xs-6 visible-xs visible-sm">
			<ul class="list-unstyled">
				<li><a href="">Sobre os Lusíadas</a></li>
				<li><a href="">Hospitais e Clínicas</a></li>
				<li><a href="">Sobre os Lusíadas</a></li>
				<li><a href="">Encontre o seu médico</a></li>
				<li><a href="">Marcações online</a></li>
				<li><a href="">Saúde A-Z</a></li>
				<li><a href="">Fale connosco</a></li>
				<li><a href="subscrever.php">Subscrever E-news</a></li>
			</ul>
		</div>
		<div class="col-md-3 visible-md visible-lg">
			<ul class="list-unstyled">
				<li><strong><a href="">Sobre os Lusíadas</a></strong></li>
				<li><a href="#">Quem Somos</a></li>
				<li><a href="#">Missão e Valores</a></li>
				<li><a href="#">Equipa de gestão</a></li>
				<li><a href="#">Responsabilidade social</a></li>
				<li><a href="#">Qualidade e prémios</a></li>
				<li><a href="#">Internation Patient Services</a></li>
				<li><a href="#">Trabalhe connosco</a></li>
				<li><a href="#">Notícias</a></li>
				<li><a href="#">Revista WE</a></li>
				<li><a href="#">Media</a></li>
			</ul>
		</div>
		<div class="col-xs-6 visible-xs visible-sm">
			<ul class="list-unstyled contacts-xs">
				<li>
					<span class="primary">800 80 00 00</span>
					<span><strong class="terciary">GRÁTIS</strong> <strong class="primary">24h</strong> <em class="primary">todos os dias</em></span>
				</li>
				<li><img src="dist/images/marcacoesFooter.png" class="img-responsive"></li>
				<li class="hidden-print"><a href="" class="icon"><i class="fa fa-facebook"></i></a> <a href="" class="icon"><i class="fa fa-youtube"></i></a></li>
			</ul>
		</div>
		<div class="col-xs-6 col-md-3 visible-md visible-lg">
			<ul class="list-unstyled">
				<li><strong><a href="">Hospitais e Clínicas</a></strong></li>
				<li><a href="#">Egestas Pharetra</a></li>
	            <li><a href="#">Egestas Pharetra</a></li>
	            <li><a href="#">Egestas Pharetra</a></li>
	            <li><a href="#">Egestas Pharetra</a></li>
	            <li><a href="#">Egestas Pharetra</a></li>
	            <li><a href="#">Egestas Pharetra</a></li>
	            <li><a href="#">Egestas Pharetra</a></li>
	            <li><a href="#">Egestas Pharetra</a></li>
	            <li><a href="#">Egestas Pharetra</a></li>
			</ul>
		</div>
		<div class="col-xs-6 col-md-3 visible-md visible-lg">
			<ul class="list-unstyled">
				<li><strong class="space"><a href="">Encontre o seu médico</a></strong></li>
				<li><strong class="space"><a href="">Marcações online</a></strong></li>
				<li><strong class="space"><a href="">Saúde A-Z</a></strong></li>
			</ul>
		</div>
		<div class="col-xs-6 col-md-3 visible-md visible-lg">
			<ul class="list-unstyled">
				<li><strong class="space"><a href="">Fale connosco</a></strong></li>
				<li><strong class="space"><a href="">Subscrever E-News</a></strong></li>
			</ul>
		</div>
	</div>

	<div class="row visible-md visible-lg">
		<div class="col-md-6">
			<span class="primary">800 80 00 00</span>
            <span><strong class="terciary">GRÁTIS</strong> <strong class="primary">24h</strong> <em class="primary">todos os dias</em></span>
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-8">
					<img src="dist/images/marcacoesFooter.png" class="img-responsive">
				</div>
				<div class="col-md-4">
					<a href="" class="icon hidden-xs"><i class="fa fa-facebook"></i></a>
            		<a href="" class="icon hidden-xs"><i class="fa fa-youtube"></i></a>
				</div>
			</div>
		</div>
	</div>
</footer>

<div class="last-row">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="center-block">
					<a href="" class="hidden-xs hidden-sm">Mapa do site |</a>
					<a href="">Todos os direitos reservados |</a>
					<a href="">Termos e condições |</a>
					<a href="">Política de cookies |</a>
					<a href="">@ 2014 Lusíadas</a>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- <div class="waiting"></div> -->

 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="dist/js/bootstrap.js"></script>
    <script src="dist/js/bootstrap-hover-dropdown.min.js"></script>
    <script src="dist/js/bootstrap-select.min.js"></script>
    <script src="dist/js/jquery.slimscroll.min.js"></script>
    <script src="dist/js/bootstrap.file-input.js"></script>
    <script src="dist/js/lusiadas.js"></script>
  </body>
</html>