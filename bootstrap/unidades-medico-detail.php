
<?php include 'header-unidades.php'; ?>

<div class="container main">
	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Hospital Lusíadas Lisboa</a></li>
				<li><a href="#">Encontre o seu médico</a></li>
				<li class="active"><a href="#">Detalhe médico</a></li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-4">
			<div class="panel">
				<img src="dist/images/medico-big.jpg" class="img-responsive">
			</div>
		</div>
		<div class="col-sm-8 main-content list-medico">
			<div class="panel detail">
					<div class="media space">
						<div class="media-body">
							<h3><a href="">Dra. Ana Pedrosa</a></h3>
							<p>Coordenadora da Unidade de Anestesiologia</p>
							<p class="title">Principais áreas de interesse:</p>
							<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
						</div>
					</div>
					<p class="title">Especialidades nesta unidade:</p>
					<h4 class="space">Anestesiologia, Cirurgia Geral</h4>
					<p class="title">Outras unidades:</p>
					<ul class="list-unstyled">
						<li><a href=""><span class="uppercase">Hospital Lusíadas Porto</span> <span class="gray">(Cirurgia Pediátrica)</span></a></li>
					</ul>

					<!-- Accordion -->
					<div class="panel-group accordion" id="accordion">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="arrow-toggle collapsed">
										Curriculum Vitae <span class="pull-right"></span>
									</a>
								</h4>
							</div>
							<div id="collapseOne" class="panel-collapse collapse">
								<div class="panel-body">
									<p>Nulla vitae elit libero, a pharetra augue. Donec ullamcorper nulla non metus auctor fringilla. Nullam quis risus eget urna mollis ornare vel eu leo. Donec id elit non mi porta gravida at eget metus.</p>
									<p>Nulla vitae elit libero, a pharetra augue. Donec ullamcorper nulla non metus auctor fringilla. Nullam quis risus eget urna mollis ornare vel eu leo. Donec id elit non mi porta gravida at eget metus.</p>
									<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Etiam porta sem malesuada magna mollis euismod. Maecenas sed diam eget risus varius blandit sit amet non magna. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Curabitur blandit tempus porttitor. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ullamcorper nulla non metus auctor fringilla. Maecenas sed diam eget risus varius blandit sit amet non magna. Curabitur blandit tempus porttitor.</p>
								</div>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 sidebar">

			<!-- Steps -->
			<ul class="nav nav-tabs steps" id="servicos-tab">
			  <li class="active"><a href="#one" data-toggle="tab">Anestesiologia</a></li>
			  <li><a href="#two" data-toggle="tab">Cirurgia Geral</a></li>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content" id="servicos-tabContent">
				<div class="tab-pane active" id="one">
					<div class="col-sm-4">
						<p class="title">Horário:</p>
						<div class="table-responsive hours">
							<table>
								<tr>
									<td class="day">SEG</td>
									<td>10:00 - 14:00 | 15:00 - 19:00</td>
								</tr>
								<tr>
									<td class="today">TER</td>
									<td>10:00 - 14:00 | 15:00 - 19:00</td>
								</tr>
								<tr>
									<td class="day">QUA</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="day">QUI</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="day">SEX</td>
									<td>10:00 - 14:00 | 15:00 - 19:00</td>
								</tr>
								<tr>
									<td class="day">SAB</td>
									<td>10:00 - 14:00 | 15:00 - 19:00</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="col-sm-8">
						<p class="title">Acordos e convenções:</p>
						<h4 class="space">Médis, Multicare, SAMS SIB, SS SGD</h4>
						<a href="" class="space-50-up"><img src="dist/images/marcar-block.jpg" class="img-responsive"></a>
					</div>
				</div>
				<div class="tab-pane" id="two">
					<div class="col-sm-4">
						<p class="title">Horário:</p>
						<div class="table-responsive hours">
							<table>
								<tr>
									<td class="day">SEG</td>
									<td>10:00 - 14:00 | 15:00 - 19:00</td>
								</tr>
								<tr>
									<td class="today">TER</td>
									<td>10:00 - 14:00 | 15:00 - 19:00</td>
								</tr>
								<tr>
									<td class="day">QUA</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="day">QUI</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="day">SEX</td>
									<td>10:00 - 14:00 | 15:00 - 19:00</td>
								</tr>
								<tr>
									<td class="day">SAB</td>
									<td>10:00 - 14:00 | 15:00 - 19:00</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="col-sm-8">
						<p class="title">Acordos e convenções:</p>
						<h4 class="space">AdvanceCare, Médis, Multicare, SAMS SIB, SS SGD</h4>
						<a href="" class="space-50-up"><img src="dist/images/marcar-block.jpg" class="img-responsive"></a>
					</div>
				</div>
			</div>

		</div>


	</div>
</div>


<?php include 'footer.php'; ?>