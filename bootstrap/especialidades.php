
<?php include 'header.php'; ?>

<div class="container main">
	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Lusiadas.pt</a></li>
				<li class="active"><a href="#">Especialidades</a></li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<h1 class="page-title">Especialidades</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-md-8 page-header">
			<p class="lead">Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec ullamcorper nulla non metus auctor fringilla. Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4 sidebar">
			<!-- Especialidades -->
			<label>Especialidades</label>
			<div class="panel fixed" id="especialidades">
				<ul class="list-unstyled">
					<li><a href="especialidades-detail.php" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
				</ul>
			</div>
		</div>

		<div class="col-md-8 main-content">
			<div class="panel text-center">
				<img src="dist/images/medicos.png" class="img-responsive">
			</div>
			<p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec ullamcorper nulla non metus auctor fringilla. Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
		</div>
	</div>
</div>


<?php include 'footer.php'; ?>