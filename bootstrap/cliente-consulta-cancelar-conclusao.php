
<?php include 'header-clients.php'; ?>

<div class="container main">
	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Portal Clientes Lusíadas</a></li>
				<li><a href="#">Consultas agendadas</a></li>
				<li><a href="#">Detalhe de Consulta</a></li>
				<li class="active"><a href="#">Cancelar Consulta</a></li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<h1 class="page-title blue">Cancelar consulta</h1>
		</div>
	</div>

	<div class="row">

		<div class="col-md-8 space clearfix">

			<div class="panel conclusion">
				<h4 class="form-title">A sua consulta foi cancelada!</h4>
				<p>Vestibulum id ligula porta felis euismod semper. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
			</div>

			<div class="row">
				<div class="col-xs-12 col-md-5 col-md-offset-7">
					<a href="cliente-consulta-cancelar.php" class="blue uppercase pull-right">Marcar Nova Consulta 
						<span class="glyphicon glyphicon-arrow-right"></span>
					</a>
				</div>
			</div>
		</div>

		<div class="col-md-4 sidebar">
			<div class="panel">
				<a href=""><img src="dist/images/consulta-ajuda.png" class="img-responsive"></a>
			</div>
			<div class="panel support">
				<span class="primary number">800 20 1000</span>
				<span><strong class="terciary">GRÁTIS</strong> <strong class="primary">24h</strong> <em class="primary">todos os dias</em></span>
    		</div>
    		<div class="panel contact-group">
    			<em>Email</em>
				<a class="primary" mailto:"geral@lusiadas.pt">geral@lusiadas.pt</a>
    		</div>

		</div>
	</div>
</div>


<?php include 'footer-clients.php'; ?>