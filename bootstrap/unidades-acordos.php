
<?php include 'header-unidades.php'; ?>

<div class="container main">
	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Hospital Lusíadas Lisboa</a></li>
				<li class="active"><a href="#">Acordos</a></li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<h1 class="page-title">Acordos</h1>
		</div>
	</div>

	<div class="row">

		<div class="col-md-8 main-content clearfix">
			<div class="page-header">
				<p class="lead">Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec ullamcorper nulla non metus auctor fringilla. Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
			</div>
			
			<!-- Accordion -->
			<div class="panel-group accordion" id="acordos">
				<div class="panel panel-default">
					<div class="panel-heading active">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#acordos" href="#collapseOne" class="arrow-toggle">
								Seguros de saúde <span class="pull-right"></span>
							</a>
						</h4>
					</div>
					<div id="collapseOne" class="panel-collapse collapse in">
						<div class="panel-body block-img">
							<div class="row space">
								<div class="col-sm-3">
									<a href=""><img src="dist/images/advance-care.png" class="img-responsive"></a>
									<a href="">AdvanceCare</a>
								</div>
								<div class="col-sm-3">
									<a href=""><img src="dist/images/advance-care.png" class="img-responsive"></a>
									<a href="">AdvanceCare</a>
								</div>
								<div class="col-sm-3">
									<a href=""><img src="dist/images/advance-care.png" class="img-responsive"></a>
									<a href="">AdvanceCare</a>
								</div>
								<div class="col-sm-3">
									<a href=""><img src="dist/images/advance-care.png" class="img-responsive"></a>
									<a href="">AdvanceCare</a>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-3">
									<a href=""><img src="dist/images/advance-care.png" class="img-responsive"></a>
									<a href="">AdvanceCare</a>
								</div>
								<div class="col-sm-3">
									<a href=""><img src="dist/images/advance-care.png" class="img-responsive"></a>
									<a href="">AdvanceCare</a>
								</div>
								<div class="col-sm-3">
									<a href=""><img src="dist/images/advance-care.png" class="img-responsive"></a>
									<a href="">AdvanceCare</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#acordos" href="#collapseTwo" class="arrow-toggle collapsed">
								Ullamcorper Amet Vulputate <span class="pull-right"></span>
							</a>
						</h4>
					</div>
					<div id="collapseTwo" class="panel-collapse collapse">
						<div class="panel-body">
							<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#acordos" href="#collapseThree" class="arrow-toggle collapsed">
								Ullamcorper Amet Vulputate <span class="pull-right"></span>
							</a> 
						</h4>
					</div>
					<div id="collapseThree" class="panel-collapse collapse">
						<div class="panel-body">
							<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-4 sidebar">
			<div class="panel">
				<img src="dist/images/hppEncontre.png" class="img-responsive">
			</div>
			<div class="panel">
				<img src="dist/images/hppMarcacoesSidebar.png" class="img-responsive">
			</div>
		</div>
	</div>
</div>


<?php include 'footer.php'; ?>