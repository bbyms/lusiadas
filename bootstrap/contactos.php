
<?php include 'header.php'; ?>

<div class="container main">
	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Lusiadas.pt</a></li>
				<li class="active"><a href="#">Contactos</a></li>
			</ol>
		</div>
	</div>


	<div class="row">
		<div class="col-md-8 main-content contact">
			<h1 class="page-title">Contactos</h1>
			<p class="lead blue">Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec ullamcorper nulla non metus auctor fringilla. Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
			
			<div class="media">
				<div class="col-xs-12 col-sm-3">
					<a href="#">
						<img class="media-object" src="dist/images/telefone.png" alt="...">
					</a>
				</div>
				<div class="media-body col-xs-12 col-sm-9">
					<span class="blue">Nº marcação único</span>
					<span class="number terciary">800 20 1000</span>
					<p>Grátis</p>
					<p>24h todos os dias</p>
				</div>
			</div>

			<div class="media">
				<div class="col-xs-12 col-sm-3">
					<a href="#">
						<img class="media-object" src="dist/images/mail.png" alt="...">
					</a>
				</div>
				<div class="media-body col-xs-12 col-sm-9">
					<span class="blue">Email</span>
					<span class="number terciary">geral@lusiadas.pt</span>
					<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Maecenas faucibus mollis interdum.</p>
				</div>
			</div>

			<div class="media">
				<div class="col-xs-12 col-sm-3">
					<a href="#">
						<img class="media-object" src="dist/images/lusiadas-contactos.jpg" alt="...">
					</a>
				</div>
				<div class="media-body col-xs-12 col-sm-9">
					<div class="row">
						<div class="col-xs-12 col-sm-8">
							<span class="blue">Online</span>
							<span class="number terciary">Portal Cliente Lusíadas</span>
							<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>
						</div>
						<div class="col-xs-12 col-sm-4 app">
							<a href=""><img src="dist/images/marcar-contactos.jpg" class="img-responsive"></a>
						</div>
					</div>
				</div>
			</div>

			<div class="media">
				<div class="col-xs-12 col-sm-3">
					<a href="#">
						<img class="media-object" src="dist/images/person.png" alt="...">
					</a>
				</div>
				<div class="media-body col-xs-12 col-sm-9">
					<span class="blue">Presencialmente nas nossas unidades</span>
					<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Maecenas faucibus mollis interdum.</p>
				</div>
			</div>
		</div>
		<div class="col-md-4 sidebar">
			<div class="panel text-center">
				<img src="dist/images/medicos.png" class="img-responsive">
			</div>
			<a href="" class="btn btn-block btn-fade-1 uppercase">Hospitais e Clínicas</a>
			<a href="" class="btn btn-block btn-fade-2 uppercase">Médicos</a>
			<a href="" class="btn btn-block btn-fade-3 uppercase">Especialidades</a>
			<a href="" class="btn btn-block btn-fade-4 uppercase">Contactos</a>
			<a href="" class="btn btn-block btn-fade-5 uppercase">Fale connosco</a>
		</div>

	</div>
</div>


<?php include 'footer.php'; ?>