
<?php include 'header.php'; ?>

<div class="container main">
	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Lusiadas.pt</a></li>
				<li class="active"><a href="#">Fale connosco</a></li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<h1 class="page-title">Fale connosco</h1>
		</div>
	</div>

	<div class="row">

		<div class="col-md-8 main-content">
			<div class="panel conclusion">
				<div class="ms-rtestate-field">
				<h4 class="form-title">Mensagem enviada!</h4>
				<p class="lead">Vestibulum id ligula porta felis euismod semper.</p>
				<p>Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
			</div>
			</div>
		</div>

		<div class="col-md-4 sidebar">
			<div class="panel contact-group">
				<h1 class="page-title">Contactos:</h1>
				<em>Telefone</em>
				<span class="terciary space">800 20 1000</span>
				<em>Email</em>
				<a class="terciary" mailto:"geral@lusiadas.pt">geral@lusiadas.pt</a>
			</div>
			<div class="panel">
				<img src="dist/images/hppEncontre.png" class="img-responsive">
			</div>
			<div class="panel">
				<img src="dist/images/hppMarcacoesSidebar.png" class="img-responsive">
			</div>
		</div>
	</div>
</div>


<?php include 'footer.php'; ?>