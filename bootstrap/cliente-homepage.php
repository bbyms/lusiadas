
<?php include 'header-clients.php'; ?>

<div class="container main">

	<div class="row">
		<div class="col-xs-12">
			<h1 class="page-title blue">Próximas marcações</h1>
		</div>
	</div>

	<div class="row">

		<div class="col-md-8 main-content clearfix">

			<!-- Alert -->
			<div class="alert alert-info">
				Não existem agendamentos.
			</div>

			<!-- Appointments rows -->

			<div class="media app first">
				<a class="pull-right" href="cliente-consulta-detalhe.php">
					<span class="glyphicon glyphicon-arrow-right"></span>
				</a>
				<div class="media-body">
					<h3>24 Março <small>16h30</small></h3>
					<span class="blue uppercase">Consulta: Cardiologia |</span> <span>Hospital Lusíadas Lisboa</span>
				</div>
			</div>

			<div class="media app second">
				<a class="pull-right" href="#">
					<span class="glyphicon glyphicon-arrow-right"></span>
				</a>
				<div class="media-body">
					<h3>24 Março <small>16h30</small></h3>
					<span class="blue uppercase">Consulta: Cardiologia |</span> <span>Hospital Lusíadas Lisboa</span>
				</div>
			</div>

			<div class="media app third">
				<a class="pull-right" href="#">
						<span class="glyphicon glyphicon-arrow-right"></span>
				</a>
				<div class="media-body">
					<h3>24 Março <small>16h30</small></h3>
					<span class="blue uppercase">Consulta: Cardiologia |</span> <span>Hospital Lusíadas Lisboa</span>
				</div>
			</div>

			<div class="clearfix">
				<a href="cliente-consulta.php" class="uppercase pull-right blue">Marcar consulta <span class="glyphicon glyphicon-arrow-right"></span></a>
			</div>

			<h1 class="page-title blue">Lusíadas Saúde mais perto de si</h1>
			<p>Nullam id dolor id nibh ultricies vehicula ut id elit. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Sed posuere consectetur est at lobortis. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec id elit non mi porta gravida at eget metus.</p>

			<p>Nullam id dolor id nibh ultricies vehicula ut id elit. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Sed posuere consectetur est at lobortis. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec id elit non mi porta gravida at eget metus.</p>
		</div>

		<div class="col-md-4 sidebar">
			<div class="panel panel-default small-calendar">
				<div class="panel-heading text-center">
					<div class="row">
						<div class="col-sm-3">
							<span class="square pull-right"></span>
						</div>
						<div class="col-sm-3 col-sm-offset-6">
							<span class="square"></span>
						</div>
					</div>
					<h3 class="uppercase">Março <small>2014</small></h3>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table>
							<thead>
								<tr>
									<th>Dom</th>
									<th>Seg</th>
									<th>Ter</th>
									<th>Qua</th>
									<th>Qui</th>
									<th>Sex</th>
									<th>Sab</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="disabled">23</td>
									<td class="disabled">24</td>
									<td class="disabled">25</td>
									<td class="disabled">26</td>
									<td class="disabled">27</td>
									<td class="disabled">28</td>
									<td class="disabled">1</td>
								</tr>
								<tr>
									<td class="disabled">23</td>
									<td class="disabled">24</td>
									<td class="disabled">25</td>
									<td class="disabled">26</td>
									<td class="disabled">27</td>
									<td class="disabled">28</td>
									<td class="disabled">1</td>
								</tr>
								<tr>
									<td class="disabled">23</td>
									<td class="disabled">24</td>
									<td class="disabled">25</td>
									<td class="disabled">26</td>
									<td class="today">27</td>
									<td>28</td>
									<td>1</td>
								</tr>
								<tr>
									<td>23</td>
									<td>24</td>
									<td class="first"><a href="">25</a></td>
									<td>26</td>
									<td>27</td>
									<td>28</td>
									<td>1</td>
								</tr>
								<tr>
									<td class="second"><a href="">23</a></td>
									<td>24</td>
									<td>25</td>
									<td>26</td>
									<td>27</td>
									<td>28</td>
									<td>1</td>
								</tr>
								<tr>
									<td>24</td>
									<td>25</td>
									<td class="third"><a href="">26</a></td>
									<td>27</td>
									<td>28</td>
									<td>1</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="panel">
				<a href=""><img src="dist/images/consulta-ajuda.png" class="img-responsive"></a>
			</div>
			<div class="panel support">
				<span class="primary number">800 20 1000</span>
				<span><strong class="terciary">GRÁTIS</strong> <strong class="primary">24h</strong> <em class="primary">todos os dias</em></span>
    		</div>
    		<div class="panel contact-group">
    			<em>Email</em>
				<a class="primary" mailto:"geral@lusiadas.pt">geral@lusiadas.pt</a>
    		</div>

		</div>
	</div>
</div>


<?php include 'footer-clients.php'; ?>