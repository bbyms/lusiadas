
<?php include 'header-unidades.php'; ?>

<div class="container main">
	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Hospital Lusíadas Lisboa</a></li>
				<li><a href="#">Apoio ao cliente</a></li>
				<li class="active"><a href="#">Marcações</a></li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<h1 class="page-title">Marcações</h1>
		</div>
	</div>

	<div class="row">

		<div class="col-md-8 main-content contact">
			<p class="lead blue">Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec ullamcorper nulla non metus auctor fringilla. Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
			<div class="media">
				<div class="col-xs-12 col-sm-3">
					<a href="#">
						<img class="media-object" src="dist/images/telefone.png" alt="...">
					</a>
				</div>
				<div class="media-body col-xs-12 col-sm-9">
					<span class="blue">Nº marcação único</span>
					<span class="number terciary">800 20 1000</span>
					<p>Grátis</p>
					<p>24h todos os dias</p>
				</div>
			</div>

			<div class="media">
				<div class="col-xs-12 col-sm-3">
					<a href="#">
						<img class="media-object" src="dist/images/mail.png" alt="...">
					</a>
				</div>
				<div class="media-body col-xs-12 col-sm-9">
					<span class="blue">Email</span>
					<span class="number terciary">geral@lusiadas.pt</span>
					<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Maecenas faucibus mollis interdum.</p>
				</div>
			</div>

			<div class="media">
				<div class="col-xs-12 col-sm-3">
					<a href="#">
						<img class="media-object" src="dist/images/lusiadas-contactos.jpg" alt="...">
					</a>
				</div>
				<div class="media-body clearfix col-xs-12 col-sm-9">
					<div class="row">
						<div class="col-xs-12 col-sm-8">
							<span class="blue">Online</span>
							<span class="number terciary">Portal Cliente Lusíadas</span>
							<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>
						</div>
						<div class="col-xs-12 col-sm-4 app">
							<a href=""><img src="dist/images/marcar-contactos.jpg" class="img-responsive"></a>
						</div>
					</div>
				</div>
			</div>

			<div class="media">
				<div class="col-xs-12 col-sm-3">
					<a href="#">
						<img class="media-object" src="dist/images/person.png" alt="...">
					</a>
				</div>
				<div class="media-body col-xs-12 col-sm-9">
					<span class="blue">Presencialmente nas nossas unidades</span>
					<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Maecenas faucibus mollis interdum.</p>
				</div>
			</div>
		</div>

		<div class="col-md-4 sidebar">
			<!-- Side nav -->
			<div class="panel-group accordion" id="unidades-nav">
				<div class="panel panel-default">
					<div class="panel-heading active">
						<a href="unidades-marcacoes.php">
							Marcações
						</a>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<a href="">
							Visitas
						</a>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<a data-toggle="collapse" data-parent="#unidades-nav" href="#guia" class="arrow-toggle collapsed">
							Guia de acolhimento <span class="pull-right"></span>
						</a> 
					</div>
					<div id="guia" class="panel-collapse collapse">
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked side-nav">
								<li><a href="">Quem Somos</a></li>
								<li class="active"><a href="">Missão e valores</a></li>
								<li><a href="">Vehicula Vulputate</a></li>
								<li><a href="">Vehicula Vulputate</a></li>
								<li><a href="">Vehicula Vulputate</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<a data-toggle="collapse" data-parent="#unidades-nav" href="#precario" class="arrow-toggle collapsed">
							Preçário e Faturação <span class="pull-right"></span>
						</a> 
					</div>
					<div id="precario" class="panel-collapse collapse">
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked side-nav">
								<li><a href="">Quem Somos</a></li>
								<li class="active"><a href="">Missão e valores</a></li>
								<li><a href="">Vehicula Vulputate</a></li>
								<li><a href="">Vehicula Vulputate</a></li>
								<li><a href="">Vehicula Vulputate</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<a href="">
							Saúde de A-Z
						</a> 
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<a href="unidades-faqs.php">
							Perguntas frequentes
						</a> 
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<a href="unidades-fale-connosco.php">
							Fale connosco
						</a> 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php include 'footer.php'; ?>