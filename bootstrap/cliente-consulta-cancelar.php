
<?php include 'header-clients.php'; ?>

<div class="container main">
	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Portal Clientes Lusíadas</a></li>
				<li><a href="#">Consultas agendadas</a></li>
				<li><a href="#">Detalhe de Consulta</a></li>
				<li class="active"><a href="#">Cancelar Consulta</a></li>
			</ol>

		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<h1 class="page-title blue">Cancelar consulta</h1>
		</div>
	</div>

	<div class="row">

		<div class="col-md-8 space clearfix">

			<!-- Steps -->
			<ul class="list-inline steps">
				<li class="active full-width">1. Confirmação</li>
			</ul>

			<!-- Alert -->
			<div class="alert alert-info">
				Tem a certeza que pretende cancelar esta consulta?
			</div>

			<div class="media space">
				<a class="pull-left" href="#">
					<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
				</a>
				<div class="media-body">
					<label>Unidade</label>
					<big>Clínica Lusíadas Parque das Nações</big>
				</div>
			</div>

			<div class="panel">
				<label>Especialidade</label>
				<big>Cardiologia</big>
			</div>

			<div class="panel">
				<label>Tipo de consulta</label>
				<big>Consulta geral</big>
			</div>

			<div class="media space">
				<a class="pull-left" href="#">
					<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
				</a>
				<div class="media-body">
					<label>Médico</label>
					<big>Dr. Afonso Vasconcelos</big>
				</div>
			</div>

			<div class="panel">
				<label>Data e hora</label>
				<big>Quarta-feira, 9 de Abril às 11:00</big>
			</div>

			<div class="clearfix">
				<div class="pull-right">
					<a href="" class="link">Manter consulta</a>
					<a href="cliente-consulta-cancelar-conclusao.php" class="btn btn-secondary uppercase">Cancelar consulta</a>
				</div>
			</div>
		</div>

		<div class="col-md-4 sidebar">
			<div class="panel">
				<a href=""><img src="dist/images/consulta-ajuda.png" class="img-responsive"></a>
			</div>
			<div class="panel support">
				<span class="primary number">800 20 1000</span>
				<span><strong class="terciary">GRÁTIS</strong> <strong class="primary">24h</strong> <em class="primary">todos os dias</em></span>
    		</div>
    		<div class="panel contact-group">
    			<em>Email</em>
				<a class="primary" mailto:"geral@lusiadas.pt">geral@lusiadas.pt</a>
    		</div>

		</div>
	</div>
</div>


<?php include 'footer-clients.php'; ?>