
<?php include 'header-clients.php'; ?>

<div class="container main">
	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Portal Clientes Lusíadas</a></li>
				<li><a href="#">Consultas agendadas</a></li>
				<li class="active"><a href="#">Detalhe de Consulta</a></li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<h1 class="page-title blue">Detalhe de consulta</h1>
		</div>
	</div>

	<div class="row">

		<div class="col-md-8 space clearfix">

			<div class="media space">
				<a class="pull-left" href="#">
					<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
				</a>
				<div class="media-body">
					<label>Unidade</label>
					<big>Clínica Lusíadas Parque das Nações</big>
				</div>
			</div>

			<!-- Accordion -->
			<div class="panel-group accordion" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="arrow-toggle collapsed">
								Localização Unidade <span class="pull-right"></span>
							</a>
						</h4>
					</div>
					<div id="collapseOne" class="panel-collapse collapse">
						<div class="panel-body">
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3111.6438043757016!2d-9.1780184!3d38.7489353!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd19332641345b89%3A0x26c22f0e2fa5543c!2sRua+Ab%C3%ADlio+Mendes!5e0!3m2!1sen!2spt!4v1398358808906" width="100%" height="450" frameborder="0" style="border:0" class="space"></iframe>
							<p class="lead blue">
								Donec sed odio dui. Aenean lacinia bibendum nulla sed consectetur. Nullam quis risus eget urna mollis ornare vel eu leo. Nullam quis risus eget urna mollis ornare vel eu leo. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.
							</p>
							<address>
								<span class="primary">Morada:</span><br>
								<p>Rua Abílio Mendes<br>
								1500-458 Lisboa</p>
							</address>
							<div class="row">
								<div class="col-sm-3 col-xs-12">
									<img src="dist/images/carro.png" class="img-responsive">
									<h4 class="blue">Como chegar de carro?</h4>
									<p>
										Donec sed odio dui. Aenean lacinia bibendum nulla sed consectetur. Nullam quis risus eget urna mollis ornare vel eu leo.
									</p>
								</div>
								<div class="col-sm-3 col-xs-12">
									<img src="dist/images/autocarro.png" class="img-responsive">
									<h4 class="blue">Como chegar de autocarro?</h4>
									<p>768 C. Universitária - Q. Olival</p>
									<p>754 Alfragide - Campo Pequeno</p>
								</div>
								<div class="col-sm-3 col-xs-12">
									<img src="dist/images/metro.png" class="img-responsive">
									<h4 class="blue">Como chegar de metro?</h4>
									<p>Estação Alto dos Moinhos - Linha Azul</p>
								</div>
								<div class="col-sm-3 col-xs-12">
									<img src="dist/images/hospital.png" class="img-responsive">
									<h4 class="blue">Quando chegar ao hospital</h4>
									<p>
										Donec sed odio dui. Aenean lacinia bibendum nulla sed consectetur. Nullam quis risus eget urna mollis ornare vel eu leo.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="panel">
				<label>Seguro/Sistema de Saúde</label>
				<big>Multicare</big>
			</div>

			<div class="panel">
				<label>Especialidade</label>
				<big>Cardiologia</big>
			</div>

			<div class="panel">
				<label>Tipo de consulta</label>
				<big>Consulta geral</big>
			</div>

			<div class="media space">
				<a class="pull-left" href="#">
					<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
				</a>
				<div class="media-body">
					<label>Médico</label>
					<big>Dr. Afonso Vasconcelos</big>
				</div>
			</div>

			<div class="panel">
				<label>Data e hora</label>
				<big>Quarta-feira, 9 de Abril às 11:00</big>
			</div>

			<div class="row text-right">
				<div class="col-xs-12 col-sm-4 col-sm-offset-4">
					<a href="cliente-consulta-cancelar.php" class="blue uppercase">Cancelar Consulta 
						<span class="glyphicon glyphicon-arrow-right"></span>
					</a>
				</div>
				<div class="col-xs-12 col-sm-4">
					<a href="" class="blue uppercase">Remarcar Consulta 
						<span class="glyphicon glyphicon-arrow-right"></span>
					</a>
				</div>
			</div>
		</div>

		<div class="col-md-4 sidebar">
			<div class="panel">
				<a href=""><img src="dist/images/consulta-ajuda.png" class="img-responsive"></a>
			</div>
			<div class="panel support">
				<span class="primary number">800 20 1000</span>
				<span><strong class="terciary">GRÁTIS</strong> <strong class="primary">24h</strong> <em class="primary">todos os dias</em></span>
    		</div>
    		<div class="panel contact-group">
    			<em>Email</em>
				<a class="primary" mailto:"geral@lusiadas.pt">geral@lusiadas.pt</a>
    		</div>

		</div>
	</div>
</div>


<?php include 'footer-clients.php'; ?>