
<?php include 'header.php'; ?>

<div class="container main">
	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Lusiadas.pt</a></li>
				<li class="active"><a href="#">Fale connosco</a></li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<h1 class="page-title">Fale connosco</h1>
		</div>
	</div>

	<div class="row">

		<div class="col-md-8 main-content clearfix">
			<div class="page-header">
				<p class="lead">Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec ullamcorper nulla non metus auctor fringilla. Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
			</div>
			<div class="alert alert-danger">
				Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec ullamcorper nulla non metus auctor fringilla.
			</div>
			<h4 class="form-title">Dados pessoais</h4>
			<form role="form">
				<div class="form-group has-error">
					<label for="name">Nome</label>
					<input type="text" class="form-control" id="name">
				</div>
				<div class="form-group row">
					<div class="col-xs-12 col-sm-6">
						<label for="email">Email</label>
						<input type="email" class="form-control" id="email">
					</div>
					<div class="col-xs-12 col-sm-6">
						<label for="phone">Telefone</label>
						<input type="text" class="form-control" id="phone">
					</div>
				</div>
				<h4 class="form-title">Mensagem</h4>
				<div class="form-group">
					<label for="subject">Assunto</label>
					<select class="selectpicker form-control" id="subject">
						<option>Pedido de informação</option>
						<option>Vulputate Sem Ligula</option>
						<option>Vulputate Sem Ligula</option>
						<option>Vulputate Sem Ligula</option>
					</select>
				</div>
				<div class="form-group">
					<label for="unidade">Unidade relacionada</label>
					<select class="selectpicker form-control" id="unidade">
						<option>Selecione uma unidade</option>
						<option>Vulputate Sem Ligula</option>
						<option>Vulputate Sem Ligula</option>
						<option>Vulputate Sem Ligula</option>
					</select>
				</div>
				<div class="form-group has-error">
					<label for="message" class="pull-left">Mensagem</label>
					<p class="help-block pull-right">Caracteres disponíveis: 250</p>
					<textarea id="message" class="form-control" rows="3"></textarea>
				</div>
				<div class="form-group">
					<div class="checkbox">
						<label>
							<input type="checkbox"> <small>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</small>
						</label>
					</div>
					<a href="" class="blue">Termos e Condições</a>
				</div>
				<div class="form-group pull-right">
					<a href="" class="link">Cancelar</a>
					<a href="conclusao.php" class="btn btn-secondary uppercase">Enviar</a>
				</div>
			</form>
		</div>

		<div class="col-md-4 sidebar">
			<div class="panel contact-group">
				<h1 class="page-title">Contactos:</h1>
				<em>Telefone</em>
				<span class="terciary space">800 20 1000</span>
				<em>Email</em>
				<a class="terciary" mailto:"geral@lusiadas.pt">geral@lusiadas.pt</a>
			</div>
			<div class="panel">
				<img src="dist/images/hppEncontre.png" class="img-responsive">
			</div>
			<div class="panel">
				<img src="dist/images/hppMarcacoesSidebar.png" class="img-responsive">
			</div>
		</div>
	</div>
</div>


<?php include 'footer.php'; ?>