
<?php include 'header-unidades.php'; ?>

<div class="container main">
	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Hospital Lusíadas Lisboa</a></li>
				<li class="active"><a href="#">Exames</a></li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<h1 class="page-title">Exames</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-md-8 page-header">
			<p class="lead">Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec ullamcorper nulla non metus auctor fringilla. Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4 sidebar">
			<!-- Especialidades -->
			<label>Especialidades</label>
			<div class="panel fixed" id="especialidades">
				<div class="panel-group accordion" id="especialidadesAccordion">
					<div class="panel panel-default">
						<div class="panel-heading">
							<a data-toggle="collapse" data-parent="#especialidadesAccordion" href="#collapseThree" class="arrow-toggle collapsed">
								Análises Clínicas <span class="pull-right"></span>
							</a> 
						</div>
						<div id="collapseThree" class="panel-collapse collapse">
							<div class="panel-body">
								<ul class="nav nav-pills nav-stacked side-nav">
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
								</ul>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading active">
							<a data-toggle="collapse" data-parent="#especialidadesAccordion" href="#collapseFour" class="arrow-toggle">
								Imagiologia <span class="pull-right"></span>
							</a> 
						</div>
						<div id="collapseFour" class="panel-collapse collapse in">
							<div class="panel-body">
								<ul class="nav nav-pills nav-stacked side-nav">
									<li><a href="">Vehicula Vulputate</a></li>
									<li class="active"><a href="unidades-exames-detail.php">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
								</ul>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<a data-toggle="collapse" data-parent="#especialidadesAccordion" href="#collapseFour" class="arrow-toggle collapsed">
								Imagiologia <span class="pull-right"></span>
							</a> 
						</div>
						<div id="collapseFour" class="panel-collapse collapse">
							<div class="panel-body">
								<ul class="nav nav-pills nav-stacked side-nav">
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
								</ul>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<a data-toggle="collapse" data-parent="#especialidadesAccordion" href="#collapseFour" class="arrow-toggle collapsed">
								Imagiologia <span class="pull-right"></span>
							</a> 
						</div>
						<div id="collapseFour" class="panel-collapse collapse">
							<div class="panel-body">
								<ul class="nav nav-pills nav-stacked side-nav">
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
								</ul>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<a data-toggle="collapse" data-parent="#especialidadesAccordion" href="#collapseFour" class="arrow-toggle collapsed">
								Imagiologia <span class="pull-right"></span>
							</a> 
						</div>
						<div id="collapseFour" class="panel-collapse collapse">
							<div class="panel-body">
								<ul class="nav nav-pills nav-stacked side-nav">
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
								</ul>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="col-md-8 main-content">
			<!-- Accordion -->
			<div class="panel-group accordion" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="arrow-toggle collapsed">
								Purus Tristique Aenean <span class="pull-right"></span>
							</a>
						</h4>
					</div>
					<div id="collapseOne" class="panel-collapse collapse">
						<div class="panel-body">
							<!-- Steps -->
							<ul class="nav nav-tabs steps" id="servicos-tab">
								<li class="active"><a href="#descricao" data-toggle="tab">Descrição</a></li>
								<li><a href="#preparacao" data-toggle="tab">Preparação</a></li>
								<li><a href="#sabermais" data-toggle="tab">Saber mais</a></li>
							</ul>
							<div class="tab-content" id="servicos-tabContent">
							<div class="tab-pane active" id="descricao">
								<p>Nulla vitae elit libero, a pharetra augue. Donec ullamcorper nulla non metus auctor fringilla. Nullam quis risus eget urna mollis ornare vel eu leo. Donec id elit non mi porta gravida at eget metus.</p>
								<img src="dist/images/slideshow1.jpg" class="img-responsive">
								<div class="default-caption">Vestibulum id ligula porta felis euismod semper.</div>
								<p>Nulla vitae elit libero, a pharetra augue. Donec ullamcorper nulla non metus auctor fringilla. Nullam quis risus eget urna mollis ornare vel eu leo. Donec id elit non mi porta gravida at eget metus.</p>
								<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Etiam porta sem malesuada magna mollis euismod. Maecenas sed diam eget risus varius blandit sit amet non magna. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Curabitur blandit tempus porttitor. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ullamcorper nulla non metus auctor fringilla. Maecenas sed diam eget risus varius blandit sit amet non magna. Curabitur blandit tempus porttitor.</p>
							</div>
						
							<div class="tab-pane" id="preparacao">

								<!-- Accordion -->
								<div class="panel-group" id="accordion">
									<p>Nullam id dolor id nibh ultricies vehicula ut id elit. Etiam porta sem malesuada magna mollis euismod. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Curabitur blandit tempus porttitor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
								</div>
							</div>

							<!-- Add class articles for styles, same as Hub Conteúdos -->
							<div class="tab-pane articles" id="sabermais">
								<div class="row space-50">
									<div class="media">
										<div class="col-xs-12 col-sm-6">
											<a class="pull-left" href="#">
												<img class="img-responsive" src="dist/images/hpEspecialidades.jpg" alt="...">
											</a>
										</div>
										<div class="media-body col-xs-12 col-sm-6">
											<span href="" class="category">Categoria</span>
											<h1 class="title"><a href="">Maecenas sed diam eget risus varius blandit</a></h1>
											<span class="author">Por Dra. Clara Pinto | 03-04-2014</span>
											<div class="ellipsis">
												<div>
													<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
													<a href="hubconteudos-detail.php">Ler mais</a>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="row space-50">
									<div class="media">
										<div class="col-xs-12 col-sm-6">
											<a class="pull-left" href="#">
												<img class="img-responsive" src="dist/images/hpEspecialidades.jpg" alt="...">
											</a>
										</div>
										<div class="media-body col-xs-12 col-sm-6">
											<span href="" class="category">Categoria</span>
											<h1 class="title"><a href="">Maecenas sed diam eget risus varius blandit</a></h1>
											<span class="author">Por Dra. Clara Pinto | 03-04-2014</span>
											<div class="ellipsis">
												<div>
													<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
													<a href="hubconteudos-detail.php">Ler mais</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						
						</div>
					</div>
				</div>
			</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="arrow-toggle collapsed">
								Ullamcorper Amet Vulputate <span class="pull-right"></span>
							</a>
						</h4>
					</div>
					<div id="collapseTwo" class="panel-collapse collapse">
						<div class="panel-body">
							<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="arrow-toggle collapsed">
								Ullamcorper Amet Vulputate <span class="pull-right"></span>
							</a> 
						</h4>
					</div>
					<div id="collapseThree" class="panel-collapse collapse">
						<div class="panel-body">
							<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php include 'footer.php'; ?>