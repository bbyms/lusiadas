
<?php include 'header-clients.php'; ?>

<div class="container main">
	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Portal Clientes Lusíadas</a></li>
				<li class="active"><a href="#">Marcar consulta</a></li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<h1 class="page-title blue">Marcar consulta</h1>
		</div>
	</div>

	<div class="row">

		<div class="col-md-8 space clearfix">

			<!-- Steps -->
			<ul class="list-inline steps">
				<li class="active">1. Dados consulta</li>
				<li>2. Data e hora</li>
				<li>3. Confirmação</li>
			</ul>

			<!-- Alert -->
			<div class="alert alert-danger">
				Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec ullamcorper nulla non metus auctor fringilla.
			</div>

			<!-- Form -->
			<form role="form">
				<div class="form-group has-error">
					<label for="unidade">Unidade</label>
					<select class="selectpicker form-control has-image" id="unidade">
						<option>Selecione uma unidade</option>
						<option data-content="<img src='dist/images/menuAlbufeira.jpg'> Vulputate Sem Ligula">Vulputate Sem Ligula</option>
						<option data-content="<img src='dist/images/menuAlbufeira.jpg'> Vulputate Sem Ligula">Vulputate Sem Ligula</option>
						<option data-content="<img src='dist/images/menuAlbufeira.jpg'> Vulputate Sem Ligula">Vulputate Sem Ligula</option>
						<option data-content="<img src='dist/images/menuAlbufeira.jpg'> Vulputate Sem Ligula">Vulputate Sem Ligula</option>
					</select>
				</div>
				<div class="form-group">
					<label for="especialidade">Especialidade</label>
					<select class="selectpicker form-control" id="especialidade">
						<option>Selecione uma especialidade</option>
						<option>Vulputate Sem Ligula</option>
						<option>Vulputate Sem Ligula</option>
						<option>Vulputate Sem Ligula</option>
					</select>
				</div>
				<div class="form-group has-error">
					<label for="tipo">Tipo de consulta</label>
					<select class="selectpicker form-control" id="tipo">
						<option>Consulta geral</option>
						<option>Vulputate Sem Ligula</option>
						<option>Vulputate Sem Ligula</option>
						<option>Vulputate Sem Ligula</option>
					</select>
				</div>
				<div class="form-group">
					<label for="medico">Médico</label>
					<select class="selectpicker form-control" id="medico">
						<option>Todos os médicos</option>
						<option>Vulputate Sem Ligula</option>
						<option>Vulputate Sem Ligula</option>
						<option>Vulputate Sem Ligula</option>
					</select>
				</div>

				<div class="pull-right">
					<a href="" class="link">Cancelar</a>
					<a href="cliente-consulta-data.php" class="btn btn-secondary uppercase">Continuar</a>
				</div>
			</form>
		</div>

		<div class="col-md-4 sidebar">
			<div class="panel">
				<a href=""><img src="dist/images/consulta-ajuda.png" class="img-responsive"></a>
			</div>
			<div class="panel support">
				<span class="primary number">800 20 1000</span>
				<span><strong class="terciary">GRÁTIS</strong> <strong class="primary">24h</strong> <em class="primary">todos os dias</em></span>
    		</div>
    		<div class="panel contact-group">
    			<em>Email</em>
				<a class="primary" mailto:"geral@lusiadas.pt">geral@lusiadas.pt</a>
    		</div>

		</div>
	</div>
</div>


<?php include 'footer-clients.php'; ?>