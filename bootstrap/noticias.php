
<?php include 'header.php'; ?>

<div class="container main">

	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Lusiadas.pt</a></li>
				<li><a href="#">Sobre os Lusíadas</a></li>
				<li class="active">Notícias</li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
			<h1 class="page-title pull-left">Notícias</h1>
		</div>
		<div class="col-md-4 text-right">
			<a href="" id="todasNoticias" data-toggle="dropdown" class="dropdown" data-hover="dropdown">
				Todas as notícias
				<span class="caret"></span>
			</a>
			<ul class="dropdown-menu text-left pull-right" role="menu" aria-labelledby="todasNoticias">
				<li role="presentation" class="active"><a role="menuitem" tabindex="-1" href="allnews.php">Todas as notícias</a></li>
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ornare Purus Commodo</a></li>
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ornare Purus Commodo</a></li>
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ornare Purus Commodo</a></li>
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ornare Purus Commodo</a></li>
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ornare Purus Commodo</a></li>
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ornare Purus Commodo</a></li>
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ornare Purus Commodo</a></li>
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ornare Purus Commodo</a></li>
			</ul>
		</div>
	</div>

	<div class="row articles">
		<div class="col-md-8 main-content">
			<div class="row">
				<div class="col-xs-12 page-header">
					<img src="dist/images/slideshow4.jpg" class="img-responsive cover-image space">
					<span class="date">11-03-2014, 12:45</span>
					<h1 class="page-title">Aenean eu leo quam. Pellentesque ornare</h1>
					<p class="lead">Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec ullamcorper nulla non metus auctor fringilla. Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
				</div>
			</div>

			<!-- Lista de conteúdos -->
			<div class="row">
				<div class="col-sm-6 col-xs-12 space-50">
					<img src="dist/images/slideshow1.jpg" class="img-responsive">
					<span class="date">11-03-2014</span>
					<h4><a href="">Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</a></h4>
					<div class="ellipsis">
						<div>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.<i></i>
						</p>
						<a href="#" class="readmore">Ler mais</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-xs-12 space-50">
					<img src="dist/images/slideshow1.jpg" class="img-responsive">
					<span class="date">11-03-2014</span>
					<h4><a href="">Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</a></h4>
					<div class="ellipsis">
						<div>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.<i></i>
						</p>
						<a href="#" class="readmore">Ler mais</a>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 col-xs-12 space-50">
					<img src="dist/images/slideshow1.jpg" class="img-responsive">
					<span class="date">11-03-2014</span>
					<h4><a href="">Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</a></h4>
					<div class="ellipsis">
						<div>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.<i></i>
						</p>
						<a href="#" class="readmore">Ler mais</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-xs-12 space-50">
					<img src="dist/images/slideshow1.jpg" class="img-responsive">
					<span class="date">11-03-2014</span>
					<h4><a href="">Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</a></h4>
					<div class="ellipsis">
						<div>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.<i></i>
						</p>
						<a href="#" class="readmore">Ler mais</a>
						</div>
					</div>
				</div>
			</div>

			<div class="media space small-list">
				<a class="pull-left" href="#">
					<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
				</a>
				<div class="media-body">
					<span class="date">11-03-2014</span>
					<h4 class="small"><a href="">Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</a></h4>
					<div class="ellipsis">
						<div>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.<i></i>
						</p>
						<a href="#" class="readmore">Ler mais</a>
						</div>
					</div>
				</div>
			</div>

			<div class="media space-50 small-list">
				<a class="pull-left" href="#">
					<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
				</a>
				<div class="media-body">
					<span class="date">11-03-2014</span>
					<h4><a href="">Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</a></h4>
					<div class="ellipsis">
						<div>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.<i></i>
						</p>
						<a href="#" class="readmore">Ler mais</a>
						</div>
					</div>
				</div>
			</div>

			<!-- End Lista conteúdos -->

			<div class="row">
				<div class="col-xs-12">
					<a href="#" class="uppercase archive">Ver todas<span class="glyphicon glyphicon-arrow-right"></span></a>
				</div>
			</div>
		</div>
		<div class="col-md-4 sidebar">
			<!-- Side nav -->
			<ul class="nav nav-pills nav-stacked side-nav">
				<li class="active"><a href="">Todas as categorias</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li class="load-more"><a href="" class="uppercase">Ver mais <span class="glyphicon glyphicon-arrow-down"></span></a></li>
			</ul>
			
			<div class="panel">
				<img src="dist/images/hppEncontre.png" class="img-responsive">
			</div>
			<div class="panel">
				<img src="dist/images/hppMarcacoesSidebar.png" class="img-responsive">
			</div>


			<!-- Top posts -->
			<div class="panel panel-default top-posts">
				<div class="panel-heading">
					<h3 class="panel-title"><img src="dist/images/mais-vistos.png"> + Vistos</h3>
				</div>
				<div class="panel-body">
					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.<i></i>
								</p>
								<a href="#" class="readmore">Ler mais</a>
								</div>
							</div>
						</div>
					</div>
					
					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.<i></i>
									</p>
									<a href="#" class="readmore">Ler mais</a>
								</div>
							</div>
						</div>
					</div>

					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.<i></i>
									</p>
									<a href="#" class="readmore">Ler mais</a>
								</div>
							</div>
						</div>
					</div>

					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.<i></i>
									</p>
									<a href="#" class="readmore">Ler mais</a>
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>
			
		</div>
	</div>

</div>


<?php include 'footer.php'; ?>