
<?php include 'header-unidades.php'; ?>

<div class="container main">
	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Hospital Lusíadas Lisboa</a></li>
				<li class="active"><a href="#">Encontre o seu médico</a></li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<h1 class="page-title">Encontre o seu médico</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-md-8 page-header">
			<p class="lead">Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec ullamcorper nulla non metus auctor fringilla. Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4 sidebar">
			<div class="input-group">
				<input type="search" placeholder="Pesquisar pelo nome do médico" class="form-control">
				<a href="" class="input-group-addon fa fa-search"></a>
			</div>

			<!-- Especialidades -->
			<label>Especialidades</label>
			<div class="panel fixed" id="medico-especialidades">
				<ul class="list-unstyled">
					<li><a href="" class="btn btn-select btn-block">Todas</a></li>
					<li><a href="unidades-especialidade.php" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
				</ul>
			</div>
		</div>

		<div class="col-md-8 main-content">
			<div class="panel-group list-medico">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<h3 class="panel-title pull-left">
							5 resultados <small>(Hospital Lusíadas Lisboa + Cardiologia Pediátrica)</small>
						</h3>
						<a href="" class="print pull-right"></a>
					</div>
					<div class="panel-body white">
						<div class="media space">
							<div class="col-xs-12 col-sm-3">
								<img class="img-responsive" src="dist/images/medico-thumb.jpg" alt="...">
							</div>
							<div class="media-body col-xs-12 col-sm-9">
								<h3>Dra. Ana Pedrosa</h3>
								<p class="title">Principais áreas de interesse:</p>
								<div class="ellipsis real">
									<div>
										<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
									</div>
								</div>
							</div>
						</div>
						<p class="title">Unidades e especialidades:</p>
						<ul class="list-unstyled">
							<li><a href=""><strong class="uppercase">Hospital Lusíadas Porto</strong> <span class="gray">(<strong>Cardiologia Pediátrica</strong>, Cirurgia Pediátrica)</span></a></li>
							<li><a href=""><span class="uppercase">Hospital Lusíadas Porto</span> <span class="gray">(<strong>Cardiologia Pediátrica</strong>)</span></a></li>
						</ul>
						<hr>
					</div>

					<div class="panel-body white">
						<div class="media space">
							<div class="col-xs-12 col-sm-3">
								<img class="img-responsive" src="dist/images/medico-thumb-default.jpg" alt="...">
							</div>
							<div class="media-body col-xs-12 col-sm-9">
								<h3>Dra. Ana Pedrosa</h3>
								<p class="title">Principais áreas de interesse:</p>
								<div class="ellipsis real">
									<div>
										<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
									</div>
								</div>
							</div>
						</div>
						<p class="title">Unidades e especialidades:</p>
						<ul class="list-unstyled">
							<li><a href=""><strong class="uppercase">Hospital Lusíadas Porto</strong> <span class="gray">(<strong>Cardiologia Pediátrica</strong>, Cirurgia Pediátrica)</span></a></li>
							<li><a href=""><span class="uppercase">Hospital Lusíadas Porto</span> <span class="gray">(<strong>Cardiologia Pediátrica</strong>)</span></a></li>
						</ul>
						<hr>
					</div>

					<div class="panel-body white">
						<div class="media space">
							<div class="col-xs-12 col-sm-3">
								<img class="img-responsive" src="dist/images/medico-thumb-default.jpg" alt="...">
							</div>
							<div class="media-body col-xs-12 col-sm-9">
								<h3>Dra. Ana Pedrosa</h3>
								<p class="title">Principais áreas de interesse:</p>
								<div class="ellipsis real">
									<div>
										<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
									</div>
								</div>
							</div>
						</div>
						<p class="title">Unidades e especialidades:</p>
						<ul class="list-unstyled">
							<li><a href=""><strong class="uppercase">Hospital Lusíadas Porto</strong> <span class="gray">(<strong>Cardiologia Pediátrica</strong>, Cirurgia Pediátrica)</span></a></li>
							<li><a href=""><span class="uppercase">Hospital Lusíadas Porto</span> <span class="gray">(<strong>Cardiologia Pediátrica</strong>)</span></a></li>
						</ul>
						<hr>
					</div>

				</div>
			</div>	
		</div>
	</div>
</div>


<?php include 'footer.php'; ?>