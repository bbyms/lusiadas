<footer class="container">
	<div class="row">
		<div class="col-xs-12">
			<strong class="space uppercase">Mapa do site</strong>
		</div>
	</div>

	<!-- Footer links -->
	<div class="row">
		<div class="col-xs-6 col-sm-4">
			<ul class="list-unstyled">
				<li><strong><a href="">Consultas</a></strong></li>
				<li><a href="">Marcar</a></li>
			</ul>
		</div>
		<div class="col-xs-6 col-sm-4">
			<ul class="list-unstyled">
				<li><strong class="space"><a href="">Fale connosco</a></strong></li>
				<li><strong class="space"><a href="">Subscrever E-News</a></strong></li>
			</ul>
		</div>
	</div>

	<div class="row hidden-xs row-xs-flex center">
		<div class="col-sm-6">
			<span class="primary">800 80 00 00</span>
            <span><strong class="terciary">GRÁTIS</strong> <strong class="primary">24h</strong> <em class="primary">todos os dias</em></span>
		</div>
		<div class="col-sm-6">
			<img src="dist/images/footer-chat.png" class="img-responsive">
		</div>
	</div>
</footer>

<div class="last-row">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="center-block">
					<a href="">Todos os direitos reservados |</a>
					<a href="">Termos e condições |</a>
					<a href="">Política de cookies |</a>
					<a href="">@ 2014 Lusíadas</a>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- <div class="waiting"></div> -->

 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="dist/js/bootstrap.js"></script>
    <script src="dist/js/bootstrap-hover-dropdown.min.js"></script>
    <script src="dist/js/bootstrap-select.min.js"></script>
    <script src="dist/js/jquery.slimscroll.min.js"></script>
    <script src="dist/js/bootstrap.file-input.js"></script>
    <script src="dist/js/lusiadas.js"></script>
  </body>
</html>