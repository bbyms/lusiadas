
<?php include 'header-unidades.php'; ?>

<div class="container main">
	
<!-- Advertisement -->
<div class="row hidden-print">
	<div class="col-xs-12">
		<figure class="add" style="background-image:url('dist/images/hpBanner.jpg');">
			<figcaption>
				<div class="row">
					<div class="col-md-7 col-sm-6 col-xs-8 col-xs-offset-4 col-sm-offset-3 col-md-offset-2">
						<h3>Aprenda a viver melhor <span class="hidden-xs hidden-sm">e a cuidar da sua saúde</span></h3>
						<em class="hidden-xs">Encontre tudo o que sempre quis saber sobre a sua saúde e a de toda a família</em>
					</div>
					<div class="col-md-3 col-sm-3 more primary hidden-xs">
						<a href="hubconteudos.php" class="btn btn-primary">Saiba mais</a>
					</div>
				</div>
			</figcaption>
		</figure>	
	</div>
</div>

<!-- Slider for Medium Up -->

<div class="row newsTabs hidden-xs carousel slide" id="myCarousel" data-ride="carousel">

	<!-- Carousel tabs -->


	<!-- Tab panes -->
	<div class="tab-content col-md-8 col-sm-8 carousel-inner">
		<div class="tab-pane fade in active item active " id="news1" >
			<figure>
				<img src="dist/images/slideshow1.jpg" class="img-responsive">
			</figure>
			<figcaption>
				<h3>Cras mattis consectetur purus sit amet fermentum.</h3>
				<div class="ellipsis">
					<div>
						<p>Etiam porta sem malesuada magna mollis euismod. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
					</div>
				</div>
			</figcaption>
		</div>
		<div class="tab-pane fade in item" id="news2">
			<figure>
				<img src="dist/images/slideshow2.jpg" class="img-responsive">
			</figure>
			<figcaption>
				<h3>Cras mattis consectetur purus sit amet fermentum.</h3>
				<div class="ellipsis">
					<div>
						<p>Etiam porta sem malesuada magna mollis euismod. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
					</div>
				</div>
			</figcaption>
		</div>
		<div class="tab-pane fade in item" id="news3">
			<figure>
				<img src="dist/images/slideshow3.jpg" class="img-responsive">
			</figure>
			<figcaption>
				<h3>Cras mattis consectetur purus sit amet fermentum.</h3>
				<div class="ellipsis">
					<div>
						<p>Etiam porta sem malesuada magna mollis euismod. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
					</div>
				</div>
			</figcaption>
		</div>
		<div class="tab-pane fade in item" id="news4">
			<figure>
				<img src="dist/images/slideshow4.jpg" class="img-responsive">
			</figure>
			<figcaption>
				<h3>Cras mattis consectetur purus sit amet fermentum.</h3>
				<div class="ellipsis">
					<div>
						<p>Etiam porta sem malesuada magna mollis euismod. Integer posuere erat.</p>
					</div>
				</div>
			</figcaption>
		</div>
	</div>
	<div class="col-md-4 col-sm-4">
		
		<ul class="nav nav-pills nav-stacked">
			<li class="active" data-target="#myCarousel" data-slide-to="0">
				<a href="#news1" data-toggle="tab">
					<span class="categoria">Amet Vulputate</span>
					<h4>Tortor Vestibulum</h4>
					<div class="ellipsis">
						<p>Aenean lacinia bibendum nulla sed consectetur.</p> 
					</div>
				</a>
			</li>
			<li data-target="#myCarousel" data-slide-to="1">
				<a href="#news2" data-toggle="tab">
					<span class="categoria">Amet Vulputate</span>
					<h4>Tortor Vestibulum</h4>
					<div class="ellipsis">
						<p>Aenean lacinia bibendum nulla sed consectetur.</p> 
					</div>
				</a>
			</li>
			<li data-target="#myCarousel" data-slide-to="2">
				<a href="#news3" data-toggle="tab">
					<span class="categoria">Amet Vulputate</span>
					<h4>Tortor Vestibulum</h4>
					<div class="ellipsis">
						<p>Aenean lacinia bibendum nulla sed consectetur.</p> 
					</div>
				</a>	
			</li>
			<li data-target="#myCarousel" data-slide-to="3">
				<a href="#news4" data-toggle="tab">
					<span class="categoria">Amet Vulputate</span>
					<h4>Tortor Vestibulum</h4>
					<div class="ellipsis">
						<p>Aenean lacinia bibendum nulla sed consectetur.</p> 
					</div>
				</a>
			</li>
		</ul>
	</div>
</div>

<!-- Slider for Medium Down -->
<div id="carousel-top" class="carousel slide hidden-sm hidden-md hidden-lg" data-ride="carousel">

	<!-- Wrapper for slides -->
	<div class="carousel-inner">
		<div class="item active">
			<img src="dist/images/slideshow1.jpg" class="img-responsive">
			<div class="carousel-caption">
				<h3>Cras mattis consectetur purus sit amet fermentum.</h3>
				<p>Etiam porta sem malesuada magna mollis euismod. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
			</div>
		</div>
		<div class="item">
			<img src="dist/images/slideshow2.jpg" class="img-responsive">
			<div class="carousel-caption">
				<h3>Cras mattis consectetur purus sit amet fermentum.</h3>
				<p>Etiam porta sem malesuada magna mollis euismod. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
			</div>
		</div>
		<div class="item">
			<img src="dist/images/slideshow3.jpg" class="img-responsive">
			<div class="carousel-caption">
				<h3>Cras mattis consectetur purus sit amet fermentum.</h3>
				<p>Etiam porta sem malesuada magna mollis euismod. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
			</div>
		</div>
		<div class="item">
			<img src="dist/images/slideshow4.jpg" class="img-responsive">
			<div class="carousel-caption">
				<h3>Cras mattis consectetur purus sit amet fermentum.</h3>
				<p>Etiam porta sem malesuada magna mollis euismod. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
			</div>
		</div>
	</div>

	<!-- Controls -->
	<a class="left carousel-control" href="#carousel-top" data-slide="prev">
		<span class="glyphicon glyphicon-arrow-left"></span>
	</a>
	<a class="right carousel-control" href="#carousel-top" data-slide="next">
		<span class="glyphicon glyphicon-arrow-right"></span>
	</a>
</div>

<!-- Blocks -->
<div class="row highlights">
	<div class="col-xs-12 col-sm-8">
		<div class="row">
			<div class="col-xs-12 col-sm-6 space-50">
				<a href=""><img src="dist/images/hpHospitais.jpg" class="img-responsive"></a>
				<h3><a href="">Hospitais e Clínicas</a></h3>
				<p>Aenean lacinia bibendum nulla sed consectetur. Vestibulum id ligula porta felis euismod semper. Cras mattis consectetur purus sit amet fermentum.</p>
			</div>
			<div class="col-xs-12 col-sm-6 space-50">
				<a href=""><img src="dist/images/hpMedicos.jpg" class="img-responsive"></a>
				<h3><a href="">Médicos</a></h3>
				<p>Aenean lacinia bibendum nulla sed consectetur. Vestibulum id ligula porta felis euismod semper. Cras mattis consectetur purus sit amet fermentum.</p>
			</div>
			<div class="col-xs-12 col-sm-6 space-50">
				<a href=""><img src="dist/images/hpLusiadasOnline.jpg" class="img-responsive"></a>
				<h3><a href="">Lusíadas Online</a></h3>
				<p>Aenean lacinia bibendum nulla sed consectetur. Vestibulum id ligula porta felis euismod semper. Cras mattis consectetur purus sit amet fermentum.</p>
			</div>
			<div class="col-xs-12 col-sm-6 space-50">
				<a href=""><img src="dist/images/hpEspecialidades.jpg" class="img-responsive"></a>
				<h3><a href="">Especialidades</a></h3>
				<p>Aenean lacinia bibendum nulla sed consectetur. Vestibulum id ligula porta felis euismod semper. Cras mattis consectetur purus sit amet fermentum.</p>
			</div>
			<div class="col-xs-12 col-sm-6">
				<div id="carousel-news" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<li data-target="#carousel-news" data-slide-to="0" class="active"></li>
						<li data-target="#carousel-news" data-slide-to="1"></li>
					</ol>

					<!-- Wrapper for slides -->
					<div class="carousel-inner">
						<div class="item active">
							<a href=""><img src="dist/images/hpNoticia1.jpg" class="img-responsive"></a>
							<h3><a href="">Notícias</a></h3>
							<div class="ellipsis">
								<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Maecenas faucibus mollis interdum. Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
								<a href="">Ler mais</a>
							</div>
						</div>
						<div class="item">
							<a href=""><img src="dist/images/hpNoticia1.jpg" class="img-responsive"></a>
							<h3><a href="">Notícias</a></h3>
							<div class="ellipsis">
								<div>
									<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Maecenas faucibus mollis interdum. Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
								<a href="">Ler mais</a>
								</div>
							</div>
						</div>		
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6">
				<a href=""><img src="dist/images/hpEspecialidades.jpg" class="img-responsive"></a>
				<h3><a href="">Especialidades</a></h3>
				<p>Aenean lacinia bibendum nulla sed consectetur. Vestibulum id ligula porta felis euismod semper. Cras mattis consectetur purus sit amet fermentum.</p>
			</div>
		</div>
	</div>


	<div class="col-xs-12 col-sm-4">
		<div class="panel space-50">
			<a href=""><img src="dist/images/hpContactos.jpg" class="img-responsive"></a>
		</div>
		<!-- Top posts -->
			<div class="panel panel-default top-posts">
				<div class="panel-heading">
					<h3 class="panel-title"><img src="dist/images/mais-vistos.png"> + Vistos</h3>
				</div>
				<div class="panel-body">
					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div>
									<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
									<a href="hubconteudos-detail.php">Ler mais</a>
								</div>
							</div>
						</div>
					</div>
					
					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div>
									<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
									<a href="hubconteudos-detail.php">Ler mais</a>
								</div>
							</div>
						</div>
					</div>

					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div>
									<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
									<a href="hubconteudos-detail.php">Ler mais</a>
								</div>
							</div>
						</div>
					</div>

					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div>
									<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
									<a href="hubconteudos-detail.php">Ler mais</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
</div>
</div>
<!-- End main container -->

<div class="container main">
	<!-- More actions -->
	<div class="row hidden-xs highlight-links">
		<div class="col-md-6 col-sm-6">
			<a href=""><img src="dist/images/medicoFooter.png" class="img-responsive pull-right"></a>
		</div>
		<div class="col-md-6 col-sm-6 ">
			<a href=""><img src="dist/images/marcacoes.png" class="img-responsive"></a>
		</div>
	</div>
</div>


<?php include 'footer.php'; ?>