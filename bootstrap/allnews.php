
<?php include 'header.php'; ?>

<div class="container main">

	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Lusiadas.pt</a></li>
				<li><a href="#">Sobre os Lusíadas</a></li>
				<li class="active">Notícias</li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
			<h1 class="page-title pull-left">Notícias</h1>
		</div>
		<div class="col-md-4 text-right">
			<a href="" id="todasNoticias" data-toggle="dropdown" class="dropdown" data-hover="dropdown">
				Todas as notícias
				<span class="caret"></span>
			</a>
			<ul class="dropdown-menu text-left pull-right" role="menu" aria-labelledby="todasNoticias">
				<li role="presentation" class="active"><a role="menuitem" tabindex="-1" href="#">Todas as notícias</a></li>
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ornare Purus Commodo</a></li>
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ornare Purus Commodo</a></li>
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ornare Purus Commodo</a></li>
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ornare Purus Commodo</a></li>
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ornare Purus Commodo</a></li>
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ornare Purus Commodo</a></li>
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ornare Purus Commodo</a></li>
				<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ornare Purus Commodo</a></li>
			</ul>
		</div>
	</div>

	<div class="row articles">
		<div class="col-md-8 main-content">
	
			<!-- Lista de conteúdos -->
		
			<div class="media space small-list">
				<a class="pull-left" href="#">
					<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
				</a>
				<div class="media-body">
					<span class="date">11-03-2014</span>
					<h4 class="small"><a href="">Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</a></h4>
					<div class="ellipsis">
						<p>Donec ullamcorper nulla non metus auctor fringilla. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
					</div>
				</div>
			</div>

			<div class="media space small-list">
				<a class="pull-left" href="#">
					<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
				</a>
				<div class="media-body">
					<span class="date">11-03-2014</span>
					<h4 class="small"><a href="">Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</a></h4>
					<div class="ellipsis">
						<p>Donec ullamcorper nulla non metus auctor fringilla. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
					</div>
				</div>
			</div>

			<div class="media space small-list">
				<a class="pull-left" href="#">
					<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
				</a>
				<div class="media-body">
					<span class="date">11-03-2014</span>
					<h4 class="small"><a href="">Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</a></h4>
					<div class="ellipsis">
						<p>Donec ullamcorper nulla non metus auctor fringilla. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
					</div>
				</div>
			</div>

			<div class="media space small-list">
				<a class="pull-left" href="#">
					<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
				</a>
				<div class="media-body">
					<span class="date">11-03-2014</span>
					<h4 class="small"><a href="">Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</a></h4>
					<div class="ellipsis">
						<p>Donec ullamcorper nulla non metus auctor fringilla. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
					</div>
				</div>
			</div>

			<div class="media space small-list">
				<a class="pull-left" href="#">
					<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
				</a>
				<div class="media-body">
					<span class="date">11-03-2014</span>
					<h4><a href="">Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</a></h4>
					<div class="ellipsis">
						<p>Donec ullamcorper nulla non metus auctor fringilla. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
					</div>
				</div>
			</div>

			<div class="media space small-list">
				<a class="pull-left" href="#">
					<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
				</a>
				<div class="media-body">
					<span class="date">11-03-2014</span>
					<h4 class="small"><a href="">Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</a></h4>
					<div class="ellipsis">
						<p>Donec ullamcorper nulla non metus auctor fringilla. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
					</div>
				</div>
			</div>

			<div class="media space small-list">
				<a class="pull-left" href="#">
					<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
				</a>
				<div class="media-body">
					<span class="date">11-03-2014</span>
					<h4 class="small"><a href="">Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</a></h4>
					<div class="ellipsis">
						<p>Donec ullamcorper nulla non metus auctor fringilla. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
					</div>
				</div>
			</div>

			<div class="media space small-list">
				<a class="pull-left" href="#">
					<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
				</a>
				<div class="media-body">
					<span class="date">11-03-2014</span>
					<h4 class="small"><a href="">Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</a></h4>
					<div class="ellipsis">
						<p>Donec ullamcorper nulla non metus auctor fringilla. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
					</div>
				</div>
			</div>

			<div class="media space small-list">
				<a class="pull-left" href="#">
					<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
				</a>
				<div class="media-body">
					<span class="date">11-03-2014</span>
					<h4 class="small"><a href="">Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</a></h4>
					<div class="ellipsis">
						<p>Donec ullamcorper nulla non metus auctor fringilla. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
					</div>
				</div>
			</div>

			<div class="media space small-list">
				<a class="pull-left" href="#">
					<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
				</a>
				<div class="media-body">
					<span class="date">11-03-2014</span>
					<h4 class="small"><a href="">Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</a></h4>
					<div class="ellipsis">
						<p>Donec ullamcorper nulla non metus auctor fringilla. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
					</div>
				</div>
			</div>

			<!-- End Lista conteúdos -->

			<div class="row">
				<div class="col-xs-12 text-center">
					<ul class="pagination">
						<li class="disabled arrow"><a href="#" class="previous"><span class="glyphicon glyphicon-arrow-left"></span> Página anterior</a></li>
						<li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li class="arrow"><a href="#" class="next">Próxima página <span class="glyphicon glyphicon-arrow-right"></span></a></li>
					</ul>
				</div>
			</div>
			
		</div>
		<div class="col-md-4 sidebar">
			<!-- Side nav -->
			<ul class="nav nav-pills nav-stacked side-nav">
				<li class="active"><a href="">Todas as categorias</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li class="load-more"><a href="" class="uppercase">Ver mais <span class="glyphicon glyphicon-arrow-down"></span></a></li>
			</ul>
			
			<div class="panel">
				<img src="dist/images/hppEncontre.png" class="img-responsive">
			</div>
			<div class="panel">
				<img src="dist/images/hppMarcacoesSidebar.png" class="img-responsive">
			</div>


			<!-- Top posts -->
			<div class="panel panel-default top-posts">
				<div class="panel-heading">
					<h3 class="panel-title"><img src="dist/images/mais-vistos.png"> + Vistos</h3>
				</div>
				<div class="panel-body">
					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div><p >Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac.</p></div>
							</div>
						</div>
					</div>
					
					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div><p >Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac.</p></div>
							</div>
						</div>
					</div>

					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div><p >Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac.</p></div>
							</div>
						</div>
					</div>

					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div><p >Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac.</p></div>
							</div>
						</div>
					</div>

				</div>
			</div>
			
		</div>
	</div>

</div>


<?php include 'footer.php'; ?>