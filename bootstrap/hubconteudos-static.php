
<?php include 'header.php'; ?>

<div class="container main">

	<!-- Advertisement -->
	<div class="row">
		<div class="col-xs-12">
			<figure class="add secondary" style="background-image:url('dist/images/hubBanner.jpg');">
				<figcaption>
					<div class="row row-xs-flex center">
						<div class="col-sm-7 col-md-8 col-xs-12 text-center">
							<h3>Bem-vindo ao hub de conteúdos Lusíadas</h3>
							<em class="hidden-sm hidden-xs">Encontre tudo o que sempre quis saber sobre a sua saúde e a de toda a família</em>
						</div>
						<div class="col-sm-5 col-md-4 more orange hidden-xs">
							<a href="" class="btn btn-orange">Subscrever E-News</a>
						</div>
					</div>
				</figcaption>
			</figure>	
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Lusiadas.pt</a></li>
				<li class="active">Hub de conteúdos</li>
			</ol>
		</div>
	</div>

	<div class="row articles">
		<div class="col-md-8 main-content">
			<div class="row">
				<div class="col-xs-12 highlight">
					<figure>
						<img src="dist/images/slideshow4.jpg" class="img-responsive cover-image">

						<figcaption class="col-xs-11 col-sm-6">
							<span href="" class="category">Categoria</span>
							<h1 class="title"><a href="">Maecenas sed diam eget risus varius blandit sit amet non magna.</a></h1>
							<span class="author">Por Dra. Clara Pinto | 03-04-2014</span>
							<div class="ellipsis">
								<div>
									<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
									<a href="hubconteudos-detail.php">Ler mais</a>
								</div>
							</div>
						</figcaption>
					</figure>
				</div>
			</div>

			<!-- Lista de conteúdos -->
			<div class="row row-xs-flex space-50">
				<!-- .cover-image para imagem ocupar 100% -->
				<div class="col-sm-6 col-xs-4 cover-image" style="background-image:url(dist/images/hpEspecialidades.jpg)">

				</div>
				<div class="col-sm-6 col-xs-8">
					<span href="" class="category">Categoria</span>
					<h1 class="title"><a href="">Maecenas sed diam eget risus varius blandit</a></h1>
					<span class="author">Por Dra. Clara Pinto | 03-04-2014</span>
					<div class="ellipsis">
						<div>
							<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
							<a href="hubconteudos-detail.php">Ler mais</a>
						</div>
					</div>
				</div>
			</div>

			<div class="row row-xs-flex space-50">
				<!-- .cover-image para imagem ocupar 100% -->
				<div class="col-sm-6 col-xs-4 cover-image" style="background-image:url(dist/images/hpEspecialidades.jpg)">
				</div>
				<div class="col-sm-6 col-xs-8">
					<span href="" class="category">Categoria</span>
					<h1 class="title"><a href="">Maecenas sed diam eget risus varius blandit</a></h1>
					<span class="author">Por Dra. Clara Pinto | 03-04-2014</span>
					<div class="ellipsis">
						<div>
							<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
							<a href="hubconteudos-detail.php">Ler mais</a>
						</div>
					</div>
				</div>
			</div>

			<div class="row row-xs-flex space-50">
				<!-- .cover-image para imagem ocupar 100% -->
				<div class="col-sm-6 col-xs-4 cover-image" style="background-image:url(dist/images/hpEspecialidades.jpg)">
				</div>
				<div class="col-sm-6 col-xs-8">
					<span href="" class="category">Categoria</span>
					<h1 class="title"><a href="">Maecenas sed diam eget risus varius blandit</a></h1>
					<span class="author">Por Dra. Clara Pinto | 03-04-2014</span>
					<div class="ellipsis">
						<div>
							<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
							<a href="hubconteudos-detail.php">Ler mais</a>
						</div>
					</div>
				</div>
			</div>

			<div class="row row-xs-flex space-50">
				<!-- .cover-image para imagem ocupar 100% -->
				<div class="col-sm-6 col-xs-4 cover-image" style="background-image:url(dist/images/hpEspecialidades.jpg)">
				</div>
				<div class="col-sm-6 col-xs-8">
					<span href="" class="category">Categoria</span>
					<h1 class="title"><a href="">Maecenas sed diam eget risus varius blandit</a></h1>
					<span class="author">Por Dra. Clara Pinto | 03-04-2014</span>
					<div class="ellipsis">
						<div>
							<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
							<a href="hubconteudos-detail.php">Ler mais</a>
						</div>
					</div>
				</div>
			</div>

			<div class="row row-xs-flex space-50">
				<!-- .cover-image para imagem ocupar 100% -->
				<div class="col-sm-6 col-xs-4 cover-image" style="background-image:url(dist/images/hpEspecialidades.jpg)">
				</div>
				<div class="col-sm-6 col-xs-8">
					<span href="" class="category">Categoria</span>
					<h1 class="title"><a href="">Maecenas sed diam eget risus varius blandit</a></h1>
					<span class="author">Por Dra. Clara Pinto | 03-04-2014</span>
					<div class="ellipsis">
						<div>
							<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
							<a href="hubconteudos-detail.php">Ler mais</a>
						</div>
					</div>
				</div>
			</div>

			<div class="row row-xs-flex space-50">
				<!-- .cover-image para imagem ocupar 100% -->
				<div class="col-sm-6 col-xs-4 cover-image" style="background-image:url(dist/images/hpEspecialidades.jpg)">
				</div>
				<div class="col-sm-6 col-xs-8">
					<span href="" class="category">Categoria</span>
					<h1 class="title"><a href="">Maecenas sed diam eget risus varius blandit</a></h1>
					<span class="author">Por Dra. Clara Pinto | 03-04-2014</span>
					<div class="ellipsis">
						<div>
							<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
							<a href="hubconteudos-detail.php">Ler mais</a>
						</div>
					</div>
				</div>
			</div>

			<div class="row row-xs-flex space-50">
				<!-- .cover-image para imagem ocupar 100% -->
				<div class="col-sm-6 col-xs-4 cover-image" style="background-image:url(dist/images/hpEspecialidades.jpg)">
				</div>
				<div class="col-sm-6 col-xs-8">
					<span href="" class="category">Categoria</span>
					<h1 class="title"><a href="">Maecenas sed diam eget risus varius blandit</a></h1>
					<span class="author">Por Dra. Clara Pinto | 03-04-2014</span>
					<div class="ellipsis">
						<div>
							<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
							<a href="hubconteudos-detail.php">Ler mais</a>
						</div>
					</div>
				</div>
			</div>

			<!-- End Lista conteúdos -->

			<div class="row">
				<div class="col-xs-12 text-center">
					<ul class="pagination">
						<li class="disabled arrow"><a href="#" class="previous"><span class="glyphicon glyphicon-arrow-left"></span> Página anterior</a></li>
						<li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li class="arrow"><a href="#" class="next">Próxima página <span class="glyphicon glyphicon-arrow-right"></span></a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-4 sidebar">
			<!-- Side nav -->
			<ul class="nav nav-pills nav-stacked side-nav">
				<li class="active"><a href="">Todas as categorias</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li class="load-more"><a href="" class="uppercase">Ver mais <span class="glyphicon glyphicon-arrow-down"></span></a></li>
			</ul>
			
			<div class="panel">
				<img src="dist/images/hppEncontre.png" class="img-responsive">
			</div>
			<div class="panel">
				<img src="dist/images/hppMarcacoesSidebar.png" class="img-responsive">
			</div>


			<!-- Top posts -->
			<div class="panel panel-default top-posts">
				<div class="panel-heading">
					<h3 class="panel-title"><img src="dist/images/mais-vistos.png"> + Vistos</h3>
				</div>
				<div class="panel-body">
					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div>
									<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
									<a href="hubconteudos-detail.php">Ler mais</a>
								</div>
							</div>
						</div>
					</div>
					
					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div>
									<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
									<a href="hubconteudos-detail.php">Ler mais</a>
								</div>
							</div>
						</div>
					</div>

					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div>
									<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
									<a href="hubconteudos-detail.php">Ler mais</a>
								</div>
							</div>
						</div>
					</div>

					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div>
									<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
									<a href="hubconteudos-detail.php">Ler mais</a>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			
		</div>
	</div>

</div>


<?php include 'footer.php'; ?>