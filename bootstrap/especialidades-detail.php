
<?php include 'header.php'; ?>

<div class="container main">
	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Lusiadas.pt</a></li>
				<li class="active"><a href="#">Especialidades</a></li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<h1 class="page-title">Especialidades</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-md-8 page-header">
			<p class="lead">Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec ullamcorper nulla non metus auctor fringilla. Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4 sidebar">
			<!-- Especialidades -->
			<label>Especialidades</label>
			<div class="panel fixed" id="especialidades">
				<ul class="list-unstyled">
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
					<li><a href="" class="btn btn-select btn-block">Analises Clinicas</a></li>
				</ul>
			</div>
		</div>

		<div class="col-md-8 main-content">
			<div class="panel-group">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">
							7 unidades com esta especialidade
						</h3>
					</div>
					<div class="panel-body white">
						<p class="title">Cardiologia nos Lusíadas:</p>
						<p>Donec ullamcorper nulla non metus auctor fringilla. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
						<p class="title">Unidades:</p>
						<ul class="list-unstyled">
							<li><a href="" class="uppercase">Hospital Lusíadas Porto</a></li>
							<li><a href="" class="uppercase">Hospital Lusíadas Porto</a></li>
							<li><a href="" class="uppercase">Hospital Lusíadas Porto</a></li>
							<li><a href="" class="uppercase">Hospital Lusíadas Porto</a></li>
						</ul>
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>


<?php include 'footer.php'; ?>