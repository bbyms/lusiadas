
<?php include 'header-unidades.php'; ?>

<div class="container main">
	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Hospital Lusíadas Lisboa</a></li>
				<li class="active"><a href="#">Exames</a></li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<h1 class="page-title">Exames</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-md-8 page-header">
			<p class="lead">Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec ullamcorper nulla non metus auctor fringilla. Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4 sidebar">
			<!-- Especialidades -->
			<label>Especialidades</label>
			<div class="panel fixed" id="especialidades">
				<div class="panel-group accordion" id="especialidadesAccordion">
					<div class="panel panel-default">
						<div class="panel-heading">
							<a data-toggle="collapse" data-parent="#especialidadesAccordion" href="#collapseThree" class="arrow-toggle collapsed">
								Análises Clínicas <span class="pull-right"></span>
							</a> 
						</div>
						<div id="collapseThree" class="panel-collapse collapse">
							<div class="panel-body">
								<ul class="nav nav-pills nav-stacked side-nav">
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
								</ul>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading active">
							<a data-toggle="collapse" data-parent="#especialidadesAccordion" href="#collapseFour" class="arrow-toggle">
								Imagiologia <span class="pull-right"></span>
							</a> 
						</div>
						<div id="collapseFour" class="panel-collapse collapse in">
							<div class="panel-body">
								<ul class="nav nav-pills nav-stacked side-nav">
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="unidades-exames-detail.php">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
								</ul>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<a data-toggle="collapse" data-parent="#especialidadesAccordion" href="#collapseFour" class="arrow-toggle collapsed">
								Imagiologia <span class="pull-right"></span>
							</a> 
						</div>
						<div id="collapseFour" class="panel-collapse collapse">
							<div class="panel-body">
								<ul class="nav nav-pills nav-stacked side-nav">
									<li><a href="">Vehicula Vulputate</a></li>
									<li class="active"><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
								</ul>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<a data-toggle="collapse" data-parent="#especialidadesAccordion" href="#collapseFour" class="arrow-toggle collapsed">
								Imagiologia <span class="pull-right"></span>
							</a> 
						</div>
						<div id="collapseFour" class="panel-collapse collapse">
							<div class="panel-body">
								<ul class="nav nav-pills nav-stacked side-nav">
									<li><a href="">Vehicula Vulputate</a></li>
									<li class="active"><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
								</ul>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<a data-toggle="collapse" data-parent="#especialidadesAccordion" href="#collapseFour" class="arrow-toggle collapsed">
								Imagiologia <span class="pull-right"></span>
							</a> 
						</div>
						<div id="collapseFour" class="panel-collapse collapse">
							<div class="panel-body">
								<ul class="nav nav-pills nav-stacked side-nav">
									<li><a href="">Vehicula Vulputate</a></li>
									<li class="active"><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
									<li><a href="">Vehicula Vulputate</a></li>
								</ul>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="col-md-8 main-content">
			<div class="panel text-center">
				<img src="dist/images/medicos.png" class="img-responsive">
			</div>
			<p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec ullamcorper nulla non metus auctor fringilla. Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
		</div>
	</div>
</div>


<?php include 'footer.php'; ?>