
<?php include 'header.php'; ?>

<div class="container main">
	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Lusiadas.pt</a></li>
				<li><a href="#">Sobre os Lusiadas</a></li>
				<li class="active">Missão e Valores</li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<h1 class="page-title">Missão e valores</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-md-8 main-content">
			<div class="panel">
				<img src="dist/images/missao.png" class="img-responsive">
			</div>
			<p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Sed posuere consectetur est at lobortis. Curabitur blandit tempus porttitor. Maecenas sed diam eget risus varius blandit sit amet non magna. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Nulla vitae elit libero, a pharetra augue.</p>

			<h5>Missão</h5>
			<p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Sed posuere consectetur est at lobortis. Curabitur blandit tempus porttitor. Maecenas sed diam eget risus varius blandit sit amet non magna. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Nulla vitae elit libero, a pharetra augue.</p>

			<h5>Valores</h5>
			<p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Sed posuere consectetur est at lobortis. Curabitur blandit tempus porttitor. Maecenas sed diam eget risus varius blandit sit amet non magna. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Nulla vitae elit libero, a pharetra augue.</p>

			<p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Sed posuere consectetur est at lobortis. Curabitur blandit tempus porttitor. Maecenas sed diam eget risus varius blandit sit amet non magna. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Nulla vitae elit libero, a pharetra augue.</p>

			<p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Sed posuere consectetur est at lobortis. Curabitur blandit tempus porttitor. Maecenas sed diam eget risus varius blandit sit amet non magna. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Nulla vitae elit libero, a pharetra augue.</p>
			<p>Cras mattis consectetur purus sit amet fermentum. Nulla vitae elit libero, a pharetra augue. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Donec sed odio dui. Aenean lacinia bibendum nulla sed consectetur.</p>
			<p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Sed posuere consectetur est at lobortis. Maecenas faucibus mollis interdum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Nullam quis risus eget urna mollis ornare vel eu leo. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec id elit non mi porta gravida at eget metus. Maecenas faucibus mollis interdum. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>

			<p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Sed posuere consectetur est at lobortis. Curabitur blandit tempus porttitor. Maecenas sed diam eget risus varius blandit sit amet non magna. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Nulla vitae elit libero, a pharetra augue.</p>
			<p>Cras mattis consectetur purus sit amet fermentum. Nulla vitae elit libero, a pharetra augue. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Donec sed odio dui. Aenean lacinia bibendum nulla sed consectetur.</p>
			<p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Sed posuere consectetur est at lobortis. Maecenas faucibus mollis interdum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Nullam quis risus eget urna mollis ornare vel eu leo. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec id elit non mi porta gravida at eget metus. Maecenas faucibus mollis interdum. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>
		
		</div>
		<div class="col-md-4">
			<!-- Side nav -->
			<ul class="nav nav-pills nav-stacked side-nav">
				<li><a href="">Quem Somos</a></li>
				<li class="active"><a href="">Missão e valores</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
			</ul>
			<div class="panel">
				<img src="dist/images/hppEncontre.png" class="img-responsive">
			</div>
			<div class="panel">
				<img src="dist/images/hppMarcacoesSidebar.png" class="img-responsive">
			</div>
			

			<!-- Top posts -->
			<div class="panel panel-default top-posts">
				<div class="panel-heading">
					<h3 class="panel-title"><img src="dist/images/mais-vistos.png"> + Vistos</h3>
				</div>
				<div class="panel-body">
					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<p>Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac. Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
								<a href="#"></a>
							</div>
						</div>
					</div>
					
					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<p>Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac. Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
								<a href="#"></a>
							</div>
						</div>
					</div>

					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<p>Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac. Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
								<a href="#"></a>
							</div>
						</div>
					</div>

					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<p>Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac. Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
								<a href="#"></a>
							</div>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>

</div>


<?php include 'footer.php'; ?>