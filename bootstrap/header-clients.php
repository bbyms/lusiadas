<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Lusíadas</title>
  <link href="dist/css/COREV15.css" rel="stylesheet" type="text/css" />
  <link href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
  <link href="dist/css/bootstrap.css" rel="stylesheet" type="text/css" />
  <link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700' rel='stylesheet' type='text/css'>
  <link rel="icon" type="icon" href="favicon.ico" />

  <script src="dist/js/modernizr.js"></script>
  <script src="dist/js/css3-mediaqueries.js"></script>

  <meta http-equiv="X-UA-Compatible" content="IE=edge"> 

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>
    <body id="s4-bodyContainer" class="clients">

      <!-- Top bar -->

      <div class="container top-links hidden-print">
        <div class="row">
          <div class="col-md-6 col-xs-6">
            <a href="" class="active idioma">PT</a> |
            <a href="" class="idioma">EN</a>
          </div>
          <div class="col-md-6 col-xs-6 top-nav text-right">
            <a href="cliente-contactos.php" class="hidden-xs">Fale connosco</a>
            <a href="" class="hidden-xs">Subscrever E-news</a>
          </div>
        </div>
      </div>

      <!-- header -->

      <div class="container main">

        <header class="row">
          <div class="col-xs-12 col-sm-6 col-md-8">
            <a href="index.php" class="logo"><img src="dist/images/logoLusiadas.png" class="img-responsive"></a>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4 user-info">
            <div class="row row-xs-flex center">
              <div class="col-xs-8 user">
                <span class="terciary uppercase">Bem-vinda</span>
                <span class="primary uppercase">Clara Ribeiro Pinto</span>
              </div>
              <div class="col-xs-4 pull-right">
                <a href=""><span class="fa fa-power-off"></span>Sair</a>
              </div>
            </div>
          </div>
        </header>

        <!--  Main navigation -->
        <div class="row">
          <nav class="navbar navbar-default yamm" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <div class="col-xs-12">
                <button type="button" class="navbar-toggle btn-block" data-toggle="collapse" data-target="#lusiadas-navbar">
                  <i class="fa fa-bars"></i>
                  <span class="text">Menu</span>
                </button>
              </div>
           </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="lusiadas-navbar">
              <ul class="nav navbar-nav">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Consultas <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li>
                      <div class="yamm-content">  
                        <div class="row">   
                          <div class="col-md-4">
                            <ul class="list-unstyled item-group">
                              <li><a href="cliente-consulta.php">Marcar consulta</a></li>
                              <li class="disabled"><a href="">Consultas agendadas</a></li>
                            </ul>
                          </div>
                          <div class="col-md-8 hidden-xs">
                            <img src="dist/images/consultas-menu.png" class="img-responsive">
                            <h4>Acesso a todas as funcionalidades</h4>
                            <div class="lsd-column">
                              Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            </div>
                          </div>
                        </div>  
                     </div>  
                  </ul>      
                </li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </nav>
        </div>
      </div>
