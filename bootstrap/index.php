
<?php include 'header.php'; ?>

<div class="container main">
	
<!-- Advertisement -->
<div class="row hidden-print">
	<div class="col-xs-12">
		<a href="hubconteudos.php">
			<figure class="add" style="background-image:url('dist/images/hpBanner.jpg');">
				<figcaption>
					<div class="row">
						<div class="col-md-7 col-sm-6 col-xs-8 col-xs-offset-4 col-sm-offset-3 col-md-offset-2">
							<h3>Aprenda a viver melhor <span class="hidden-xs hidden-sm">e a cuidar da sua saúde</span></h3>
							<em class="hidden-xs">Encontre tudo o que sempre quis saber sobre a sua saúde e a de toda a família</em>
						</div>
						<div class="col-md-3 col-sm-3 more info hidden-xs">
							<a href="hubconteudos.php" class="btn btn-info">Saiba mais</a>
						</div>
					</div>
				</figcaption>
			</figure>
		</a>	
	</div>
</div>

<!-- Slider for Medium Up -->

<div class="row newsTabs hidden-xs carousel slide" id="myCarousel" data-ride="carousel">

	<!-- Carousel tabs -->


	<!-- Tab panes -->
	<div class="tab-content col-md-8 col-sm-8 carousel-inner">
		<div class="tab-pane active item active " id="news1" >
			<figure>
				<img src="dist/images/slideshow1.jpg" class="img-responsive">
			</figure>
			<figcaption>
				<h3>Cras mattis consectetur purus sit amet fermentum.</h3>
			</figcaption>
		</div>
		<div class="tab-pane item" id="news2">
			<figure>
				<img src="dist/images/slideshow2.jpg" class="img-responsive">
			</figure>
			<figcaption>
				<h3>Cras mattis consectetur purus sit amet fermentum.</h3>
			</figcaption>
		</div>
		<div class="tab-pane item" id="news3">
			<figure>
				<img src="dist/images/slideshow3.jpg" class="img-responsive">
			</figure>
			<figcaption>
				<h3>Cras mattis consectetur purus sit amet fermentum.</h3>
			</figcaption>
		</div>
		<div class="tab-pane item" id="news4">
			<figure>
				<img src="dist/images/slideshow4.jpg" class="img-responsive">
			</figure>
			<figcaption>
				<h3>Cras mattis consectetur purus sit amet fermentum.</h3>
			</figcaption>
		</div>
	</div>
	<div class="col-md-4 col-sm-4">
		
		<ul class="nav nav-pills nav-stacked">
			<li class="active" data-target="#myCarousel" data-slide-to="0">
				<a href="" data-toggle="tab">
					<span class="categoria">Amet Vulputate</span>
					<h4>Tortor Vestibulum</h4>
					<div class="ellipsis">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
						</p>
					</div>
				</a>
			</li>
			<li data-target="#myCarousel" data-slide-to="1">
				<a href="" data-toggle="tab">
					<span class="categoria">Amet Vulputate</span>
					<h4>Tortor Vestibulum</h4>
					<div class="ellipsis">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
						</p>
					</div>
				</a>
			</li>
			<li data-target="#myCarousel" data-slide-to="2">
				<a href="" data-toggle="tab">
					<span class="categoria">Amet Vulputate</span>
					<h4>Tortor Vestibulum</h4>
					<div class="ellipsis">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
						</p>
					</div>
				</a>	
			</li>
			<li data-target="#myCarousel" data-slide-to="3">
				<a href="" data-toggle="tab">
					<span class="categoria">Amet Vulputate</span>
					<h4>Tortor Vestibulum</h4>
					<div class="ellipsis">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
						</p>
					</div>
				</a>
			</li>
		</ul>
	</div>
</div>

<!-- Slider for Medium Down -->
<div id="carousel-top" class="carousel slide hidden-sm hidden-md hidden-lg" data-ride="carousel">

	<!-- Wrapper for slides -->
	<div class="carousel-inner">
		<div class="item active">
			<img src="dist/images/slideshow1.jpg" class="img-responsive">
			<div class="carousel-caption">
				<h3>Cras mattis consectetur purus sit amet fermentum.</h3>
				<p>Etiam porta sem malesuada magna mollis euismod. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
			</div>
		</div>
		<div class="item">
			<img src="dist/images/slideshow2.jpg" class="img-responsive">
			<div class="carousel-caption">
				<h3>Cras mattis consectetur purus sit amet fermentum.</h3>
				<p>Etiam porta sem malesuada magna mollis euismod. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
			</div>
		</div>
		<div class="item">
			<img src="dist/images/slideshow3.jpg" class="img-responsive">
			<div class="carousel-caption">
				<h3>Cras mattis consectetur purus sit amet fermentum.</h3>
				<p>Etiam porta sem malesuada magna mollis euismod. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
			</div>
		</div>
		<div class="item">
			<img src="dist/images/slideshow4.jpg" class="img-responsive">
			<div class="carousel-caption">
				<h3>Cras mattis consectetur purus sit amet fermentum.</h3>
				<p>Etiam porta sem malesuada magna mollis euismod. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
			</div>
		</div>
	</div>

	<!-- Controls -->
	<a class="left carousel-control" href="#carousel-top" data-slide="prev">
		<span class="glyphicon glyphicon-arrow-left"></span>
	</a>
	<a class="right carousel-control" href="#carousel-top" data-slide="next">
		<span class="glyphicon glyphicon-arrow-right"></span>
	</a>
</div>

<!-- Blocks -->
<div class="row highlights">
	<div class="col-xs-12 col-sm-6 col-md-4">
		<a href=""><img src="dist/images/hpHospitais.jpg" class="img-responsive"></a>
		<h3><a href="hospitais-clinicas.php">Hospitais e Clínicas</a></h3>
		<div class="">
			<p>Aenean lacinia bibendum nulla sed consectetur. Vestibulum id ligula porta felis euismod semper. Cras mattis consectetur purus sit amet fermentum.</p>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<a href=""><img src="dist/images/hpMedicos.jpg" class="img-responsive"></a>
		<h3><a href="">Médicos</a></h3>
		<div class="">
			<p>Aenean lacinia bibendum nulla sed consectetur. Vestibulum id ligula porta felis euismod semper. Cras mattis consectetur purus sit amet fermentum.</p>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<a href=""><img src="dist/images/hpLusiadasOnline.jpg" class="img-responsive"></a>
		<h3><a href="">Lusíadas Online</a></h3>
		<div class="">
			<p>Aenean lacinia bibendum nulla sed consectetur. Vestibulum id ligula porta felis euismod semper. Cras mattis consectetur purus sit amet fermentum.</p>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<a href=""><img src="dist/images/hpEspecialidades.jpg" class="img-responsive"></a>
		<h3><a href="especialidades.php">Especialidades</a></h3>
		<div class="">
			<p>Aenean lacinia bibendum nulla sed consectetur. Vestibulum id ligula porta felis euismod semper. Cras mattis consectetur purus sit amet fermentum.</p>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div id="carousel-news" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#carousel-news" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-news" data-slide-to="1"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<div class="item active">
					<a href=""><img src="dist/images/hpNoticia1.jpg" class="img-responsive"></a>
					<h3 class="oneline"><a href="">Isto é um título extenso de uma notícia</a></h3>
					<div class="ellipsis">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
							<a href="" class="readmore">Ler mais</a>
						</p>
					</div>
				</div>
				<div class="item">
					<a href=""><img src="dist/images/hpNoticia1.jpg" class="img-responsive"></a>
					<h3 class="oneline"><a href="">Notícias</a></h3>
					<div class="ellipsis">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
							<a href="" class="readmore">Ler mais</a>
						</p>
					</div>
				</div>	
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<a href="contactos.php"><img src="dist/images/hpContactos.jpg" class="img-responsive"></a>
	</div>
</div>
</div>
<!-- End main container -->

<div class="container latest-news">
	<!-- News -->
	<div class="row hidden-xs">
		<div class="col-sm-12">
			<h3 class="area-title"><img src="dist/images/lastNews.png"> Últimas</h3>
		</div>
		<div class="col-sm-4 col-md-3">
			<a href=""><img src="dist/images/hpUltimas1.jpg" class="img-responsive"></a>
			<h4><a href="">Aprender a não ter medo</a></h4>
			<div class="ellipsis">
				<p>Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac. Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
				<a href="#"></a>
			</div>
		</div>
		<div class="col-sm-4 col-md-3">
			<a href=""><img src="dist/images/hpUltimas2.jpg" class="img-responsive"></a>
			<h4><a href="">Cuidades de saúde aos 50</a></h4>
			<div class="ellipsis">
				<p>Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac. Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
				<a href="#"></a>
			</div>
		</div>
		<div class="col-sm-4 col-md-3">
			<a href=""><img src="dist/images/hpUltimas3.jpg" class="img-responsive"></a>
			<h4><a href="">Evitar o baby blues</a></h4>
			<div class="ellipsis">
				<p>Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac. Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
				<a href="#"></a>
			</div>
		</div>
		<div class="col-sm-4 col-md-3 hidden-sm">
			<a href=""><img src="dist/images/hpUltimas4.jpg" class="img-responsive"></a>
			<h4><a href="">O papel dos cuidadores</a></h4>
			<div class="ellipsis">
				<p>Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac. Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
				<a href="#"></a>
			</div>
		</div>
	</div>
</div>

<div class="container main">
	<!-- More actions -->
	<div class="row hidden-xs highlight-links">
		<div class="col-md-6 col-sm-6">
			<a href=""><img src="dist/images/medicoFooter.png" class="img-responsive pull-right"></a>
		</div>
		<div class="col-md-6 col-sm-6 ">
			<a href=""><img src="dist/images/marcacoes.png" class="img-responsive"></a>
		</div>
	</div>
</div>


<?php include 'footer.php'; ?>