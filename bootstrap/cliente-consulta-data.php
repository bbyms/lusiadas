
<?php include 'header-clients.php'; ?>

<div class="container main">
	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Portal Clientes Lusíadas</a></li>
				<li class="active"><a href="#">Marcar consulta</a></li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<h1 class="page-title blue">Marcar consulta</h1>
		</div>
	</div>

	<div class="row">

		<div class="col-md-8 space clearfix">

			<!-- Steps -->
			<ul class="list-inline steps">
				<li class="active">1. Dados consulta</li>
				<li class="active">2. Data e hora</li>
				<li>3. Confirmação</li>
			</ul>

			<!-- Info -->
			<div class="alert alert-info">
				Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec ullamcorper nulla non metus auctor fringilla.
			</div>

			<!-- Back -->

			<div class="clearfix">
				<a href="cliente-consulta.php" class="uppercase pull-right blue space">Alterar dados consulta <span class="glyphicon glyphicon-arrow-left"></span></a>
			</div>

			<!-- Choose date -->

<!-- 			<div class="row">
				<div class="col-xs-12">
					<label class="primary">Selecione o horário pretendido</label>
				</div>
			</div> -->

			<!-- This -->
			<div class="row">
				<div class="col-xs-12">
					<span class="primary thin">Horário selecionado</span>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<label class="primary">Segunda-feira, 07 de Abril às 11:00</label>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<a href="" class="uppercase blue">Ver 1ª vaga disponível</a> |
					<a href="" class="uppercase blue">Ver data selecionada</a>
				</div>
			</div>

			<!-- or that -->
			<div class="row">
				<div class="col-xs-12">
					<span class="primary thin">Horário selecionado (data sugerida com base na próxima vaga disponível)</span>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<label class="primary">Segunda-feira, 07 de Abril às 11:00</label>
				</div>
			</div>
			
			<!-- Calendário-->
			<div class="table-responsive">
				
			<table class="calendar hidden-xs">
				<thead>
					<tr>
						<th class="nav prev">
							<a href="" class="glyphicon glyphicon-arrow-left"></a>
						</th>
						<th colspan="6">
							<h5 class="uppercase">Maio 2014</h5>
						</th>
						<th class="nav next">
							<a href="" class="glyphicon glyphicon-arrow-right"></a>
						</th>
					</tr>
					<tr>
						<th class="nav prev">
							<a href="" class="glyphicon glyphicon-arrow-left"></a>
						</th>
						<th>segunda <span id="ctl00_ctl00_PlaceHolderMain_PlaceHolderMain_ctl00_lblDoW1">12 mai</span> </th>
						<th>terça <span id="ctl00_ctl00_PlaceHolderMain_PlaceHolderMain_ctl00_lblDoW2">13 mai</span> </th>
						<th>quarta <span id="ctl00_ctl00_PlaceHolderMain_PlaceHolderMain_ctl00_lblDoW3">14 mai</span> </th>
						<th class="selectable">quinta <span id="ctl00_ctl00_PlaceHolderMain_PlaceHolderMain_ctl00_lblDoW4">15 mai</span> </th>
						<th>sexta <span id="ctl00_ctl00_PlaceHolderMain_PlaceHolderMain_ctl00_lblDoW5">16 mai</span> </th>
						<th>sábado <span id="ctl00_ctl00_PlaceHolderMain_PlaceHolderMain_ctl00_lblDoW6">17 mai</span> </th>
						<th class="nav next">
							<a href="" class="glyphicon glyphicon-arrow-right"></a>
						</th>
					</tr>
				</thead>
				<tbody id="ctl00_ctl00_PlaceHolderMain_PlaceHolderMain_ctl00_tbdySchedule">
					<tr>
						<td class="hour">09:00</td>
						<td class="selectable" data-date="20140512090000"></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td class="blank"></td>
					</tr>
					<tr>
						<td class="hour">10:00</td>
						<td class="selectable cal-trigger" data-date="20140512100000">
							3 vagas
							<div class="content hide">
								10:10 <br>
								10:20 <br>
								10:40
							</div>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td class="blank"></td>
					</tr>
					<tr>
						<td class="hour">11:00</td>
						<td class="selectable" data-date="20140512110000"></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td class="blank"></td>
					</tr>
					<tr>
						<td class="hour">12:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td class="selected cal-trigger">
							3 vagas
							<div class="content hide">
								<span class="secondary-bg">10:10</span><br>
								<span>10:20</span><br>
								<span>10:40</span>
							</div>
						</td>
						<td></td>
						<td></td>
						<td class="blank"></td>
					</tr>
					<tr>
						<td class="hour">13:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td class="blank"></td>
					</tr>
					<tr>
						<td class="hour">14:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td class="blank"></td>
					</tr>
					<tr>
						<td class="hour">15:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td class="blank"></td>
					</tr>
					<tr>
						<td class="hour">16:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td class="blank"></td>
					</tr>
					<tr>
						<td class="hour">17:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td class="blank"></td>
					</tr>
					<tr>
						<td class="hour">18:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td class="blank"></td>
					</tr>
					<tr>
						<td class="hour">19:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td class="blank"></td>
					</tr>
					<tr>
						<td class="hour">20:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td class="blank"></td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td></td>
						<td colspan="3">
							<span class="selectable"></span>
							Horários disponíveis
						</td>
						<td colspan="3">
							<span class="selected"></span>
							Horário selecionado
						</td>
					</tr>
				</tfoot>
			</table>

			<table class="calendar visible-xs">
				<thead>
					<tr>
						<th class="nav prev">
							<a href="" class="glyphicon glyphicon-arrow-left"></a>
						</th>
						<th>
							<!-- <h3 class="uppercase">Maio</h3> -->
							<span>Semana de 12 a 17 Maio</span>
						</th>
						<th class="nav next">
							<a href="" class="glyphicon glyphicon-arrow-right"></a>
						</th>
					</tr>
					
				</thead>
				<tbody id="ctl00_ctl00_PlaceHolderMain_PlaceHolderMain_ctl00_tbdySchedule">
					<tr>
						<th colspan="3">segunda <span id="ctl00_ctl00_PlaceHolderMain_PlaceHolderMain_ctl00_lblDoW1">12 mai</span> </th>
					</tr>
					<tr>
						<td colspan="3" class="selectable" data-date="20140512090000">09:00</td>
					</tr>
					<tr>
						<td colspan="3" class="selectable" data-date="20140512090000">10:00</td>
					</tr>
					<tr>
						<td colspan="3" class="selectable" data-date="20140512090000">11:00</td>
					</tr>
					<tr>
						<th colspan="3">terça <span id="ctl00_ctl00_PlaceHolderMain_PlaceHolderMain_ctl00_lblDoW1">13 mai</span> </th>
					</tr>
					<tr>
						<td colspan="3" class="selected" data-date="20140512090000">09:00</td>
					</tr>
					<tr>
						<th colspan="3">quarta <span id="ctl00_ctl00_PlaceHolderMain_PlaceHolderMain_ctl00_lblDoW1">14 mai</span> </th>
					</tr>
					<tr>
						<td colspan="3" class="selectable" data-date="20140512090000">09:00</td>
					</tr>
					<tr>
						<td colspan="3" class="selectable" data-date="20140512090000">10:00</td>
					</tr>
					<tr>
						<td colspan="3" class="selectable" data-date="20140512090000">11:00</td>
					</tr>
				</tbody>

			</table>
		</div>

			<div class="clearfix">
				<div class="pull-right">
					<a href="" class="link">Cancelar</a>
					<a href="cliente-consulta-confirmacao.php" class="btn btn-secondary uppercase">Continuar</a>	
				</div>
			</div>
		</div>

		<div class="col-md-4 sidebar">
			<div class="panel">
				<a href=""><img src="dist/images/consulta-ajuda.png" class="img-responsive"></a>
			</div>
			<div class="panel support">
				<span class="primary number">800 20 1000</span>
				<span><strong class="terciary">GRÁTIS</strong> <strong class="primary">24h</strong> <em class="primary">todos os dias</em></span>
    		</div>
    		<div class="panel contact-group">
    			<em>Email</em>
				<a class="primary" mailto:"geral@lusiadas.pt">geral@lusiadas.pt</a>
    		</div>

		</div>

		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title thin" id="myModalLabel">Selecione o horário pretendido</h4>
					</div>
					<div class="modal-body">
						<div class="radio">
							<label>
								<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
								10:10
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
								10:20
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
								10:40
							</label>
						</div>
					</div>
					<div class="modal-footer">
						<a class="link" data-dismiss="modal">Cancelar</a>
						<a class="btn btn-secondary uppercase">Gravar</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php include 'footer-clients.php'; ?>