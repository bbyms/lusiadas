<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Lusíadas</title>
  <link href="dist/css/COREV15.css" rel="stylesheet" type="text/css" />
  <link href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
  <link href="dist/css/bootstrap.css" rel="stylesheet" type="text/css" />
  <link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700' rel='stylesheet' type='text/css'>
  <link rel="icon" type="image/png" href="dist/images/favicon.png" />

  <script src="dist/js/modernizr.js"></script>
  <script src="dist/js/css3-mediaqueries.js"></script>

  <meta http-equiv="X-UA-Compatible" content="IE=edge"> 

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>
    <body id="s4-bodyContainer">
      <div id="fb-root"></div>
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=279165555438827";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>
      <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
          

      <!-- Top bar -->

      <div class="container top-links hidden-print">
        <div class="row">
          <div class="col-md-2 col-sm-2 col-xs-3">
            <a href="" class="active idioma">PT</a> |
            <a href="" class="idioma">EN</a>
          </div>
          <div class="col-md-7 col-sm-5 col-xs-7 top-nav">
            <a href="index.php"><span class="glyphicon glyphicon-home"></span><strong>Lusiadas.pt</strong></a>
            <a href="index-unidades.php" id="unidades" data-toggle="dropdown" class="dropdown-toggle" data-hover="dropdown">Unidades<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="unidades">
              <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Hospital Lusíadas Porto</a></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Hospital Lusíadas Lisboa</a></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Clínica Lusíadas Parque das Nações</a></li>
            </ul>
            <a href="fale-connosco.php" class="hidden-xs hidden-sm">Fale connosco</a>
            <a href="" class="hidden-xs hidden-sm">Subscrever E-news</a>
            <a href="" class="icon hidden-xs"><i class="fa fa-facebook"></i></a>
            <a href="" class="icon hidden-xs"><i class="fa fa-youtube"></i></a>
          </div>
          <div class="col-md-3 col-sm-5 col-xs-2 text-right">
            <input type="search" placeholder="O que procura?" class="hidden-xs">
            <button type="submit" class="icon search"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </div>


      <!-- header -->

      <div class="container main">

        <header class="row">
          <div class="col-md-7 col-sm-6 col-xs-5">
            <a href="index.php" class="logo"><img src="dist/images/logoLusiadas.png" class="img-responsive"></a>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-7 support">
            <span class="terciary text-right">800 80 00 00</span>
            <span class="text-right"><strong class="terciary">GRÁTIS</strong> <strong class="primary">24h</strong> <em class="primary">todos os dias</em></span>
             <a href="cliente-login.php" class="btn btn-secondary btn-block visible-sm space-20-up"><img src="dist/images/logoMaisLusiadas.png"></a>
             <span class="is-login visible-sm">Login Cliente</span>
         </div>
        </header>

        <!--  Main navigation -->
        <div class="row">
          <nav class="navbar navbar-default yamm public" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header row-xs-flex">
              <div class="col-xs-3">
                <button type="button" class="navbar-toggle btn-block" data-toggle="collapse" data-target="#lusiadas-navbar">
                  <i class="fa fa-bars"></i>
     <!--              <span class="text">Menu</span> -->
                </button>
              </div>
              <div class="col-xs-9">
                 <a href="cliente-login.php" class="btn btn-secondary visible-xs"><img src="dist/images/logoMaisLusiadas.png"></a>
                 <span class="is-login visible-xs">Login Cliente</span>
             </div>
           </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="lusiadas-navbar">
              <ul class="nav navbar-nav">
                <li class="dropdown">
                  <a href="unidades-boas.php" class="dropdown" data-hover="dropdown">O Hospital <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li>
                      <div class="yamm-content">  
                        <div class="row">   
                          <div class="col-md-4">
                            <ul class="list-unstyled item-group">
                              <li>
                                <a href="#">Quem Somos</a>
                              </li>
                              <li class="secondary"><a href="">Venenatis Inceptos</a></li>
                              <li class="secondary"><a href="">Ultricies Risus</a></li>
                              <li><a href="missao.php">Missão e Valores</a></li>
                              <li><a href="#">Equipa de gestão</a></li>
                              <li><a href="#">Responsabilidade social</a></li>
                              <li><a href="#">Qualidade e prémios</a></li>
                            </ul>
                          </div>
                          <div class="col-md-4">
                            <ul class="list-unstyled item-group">
                              <li><a href="#">Internation Patient Services</a></li>
                              <li><a href="#">Trabalhe connosco</a></li>
                              <li><a href="noticias.php">Notícias</a></li>
                              <li><a href="#">Revista WE</a></li>
                              <li><a href="#">Media</a></li>
                            </ul>
                          </div>
                          <div class="col-md-4 hidden-xs">
                            <img src="dist/images/noticia-menu.png" class="img-responsive">
                            <h4>Destaque Notícia</h4>
                            <p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Maecenas sed diam eget risus varius blandit sit amet non magna. Cras mattis consectetur purus sit amet fermentum. <a href="">Ler mais</a></p>
                          </div>
                        </div>  
                     </div>  
                  </ul>      
                </li>
                <li class="dropdown">
                  <a href="unidades-servicos.php" class="dropdown" data-hover="dropdown">Serviços Clínicos <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li>
                      <div class="yamm-content">  
                        <div class="row">   

                          <!-- First row -->
                          <div class="col-md-4">

                            <ul class="list-unstyled">
                              <li class="item-group">
                                <div class="row">
                                  <div class="col-md-4">
                                    <img src="dist/images/menuAlbufeira.jpg">
                                  </div>
                                  <div class="col-md-8">
                                    <a href="#">Hospital Condimentum Ligula</a>
                                  </div>
                                </div>
                              </li>
                              <li class="item-group">
                                <div class="row">
                                  <div class="col-md-4">
                                    <img src="dist/images/menuAlbufeira.jpg">
                                  </div>
                                  <div class="col-md-8">
                                    <a href="">Hospital Condimentum Ligula</a>
                                  </div>
                                </div>
                              </li>
                              <li class="item-group">
                                <div class="row">
                                  <div class="col-md-4">
                                    <img src="dist/images/menuAlbufeira.jpg">
                                  </div>
                                  <div class="col-md-8">
                                    <a href="">Hospital Condimentum Ligula</a>
                                  </div>
                                </div>
                              </li>
                              <li class="item-group">
                                <div class="row">
                                  <div class="col-md-4">
                                    <img src="dist/images/menuAlbufeira.jpg">
                                  </div>
                                  <div class="col-md-8">
                                    <a href="">Hospital Condimentum Ligula</a>
                                  </div>
                                </div>
                              </li>
                            </ul>

                          </div>

                          <!-- second row -->
                          <div class="col-md-4">

                            <ul class="list-unstyled">
                              <li class="item-group">
                                <div class="row">
                                  <div class="col-md-4">
                                    <img src="dist/images/menuAlbufeira.jpg">
                                  </div>
                                  <div class="col-md-8">
                                    <a href="#">Hospital Condimentum Ligula</a>
                                  </div>
                                </div>
                              </li>
                              <li class="item-group">
                                <div class="row">
                                  <div class="col-md-4">
                                    <img src="dist/images/menuAlbufeira.jpg">
                                  </div>
                                  <div class="col-md-8">
                                    <a href="">Hospital Condimentum Ligula</a>
                                  </div>
                                </div>
                              </li>
                              <li class="item-group">
                                <div class="row">
                                  <div class="col-md-4">
                                    <img src="dist/images/menuAlbufeira.jpg">
                                  </div>
                                  <div class="col-md-8">
                                    <a href="">Hospital Condimentum Ligula</a>
                                  </div>
                                </div>
                              </li>
                              <li class="item-group">
                                <div class="row">
                                  <div class="col-md-4">
                                    <img src="dist/images/menuAlbufeira.jpg">
                                  </div>
                                  <div class="col-md-8">
                                    <a href="">Hospital Condimentum Ligula</a>
                                  </div>
                                </div>
                              </li>
                            </ul>

                          </div>

                          <!-- third row -->
                          <div class="col-md-4">

                            <ul class="list-unstyled">
                              <li class="item-group">
                                <div class="row">
                                  <div class="col-md-4">
                                    <img src="dist/images/menuAlbufeira.jpg">
                                  </div>
                                  <div class="col-md-8">
                                    <a href="#">Hospital Condimentum Ligula</a>
                                  </div>
                                </div>
                              </li>
                              <li class="item-group">
                                <div class="row">
                                  <div class="col-md-4">
                                    <img src="dist/images/menuAlbufeira.jpg">
                                  </div>
                                  <div class="col-md-8">
                                    <a href="">Hospital Condimentum Ligula</a>
                                  </div>
                                </div>
                              </li>
                              <li class="item-group">
                                <div class="row">
                                  <div class="col-md-4">
                                    <img src="dist/images/menuAlbufeira.jpg">
                                  </div>
                                  <div class="col-md-8">
                                    <a href="">Hospital Condimentum Ligula</a>
                                  </div>
                                </div>
                              </li>
                              <li class="item-group">
                                <div class="row">
                                  <div class="col-md-4">
                                    <img src="dist/images/menuAlbufeira.jpg">
                                  </div>
                                  <div class="col-md-8">
                                    <a href="">Hospital Condimentum Ligula</a>
                                  </div>
                                </div>
                              </li>
                            </ul>

                          </div>
                        </div>
                          <div class="row">
                            <div class="col-md-12 text-right">
                              <hr>
                              <a href="">Todos os hospitais e clínicas</a>
                            </div>
                          </div>
                        </div>  
                  </ul>
                </li>
                <li><a href="unidades-medico.php">Encontre o seu médico</a></li>
                <li><a href="unidades-marcacoes.php">Apoio ao cliente</a></li>
                <li><a href="unidades-acordos.php">Acordos</a></li>
                <li class="hidden-xs hidden-sm">
                  <a href="cliente-login.php" class="portal"><img src="dist/images/logoMaisLusiadas.png"></a>
                  <span class="is-login">Login Cliente</span>
                </li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </nav>
        </div>
      </div>
