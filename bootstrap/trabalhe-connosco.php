
<?php include 'header.php'; ?>

<div class="container main">
	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Lusiadas.pt</a></li>
				<li><a href="#">Sobre os Lusiadas</a></li>
				<li class="active"><a href="#">Trabalhe connosco</a></li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<h1 class="page-title">Trabalhe connosco</h1>
		</div>
	</div>

	<div class="row">

		<div class="col-md-8 main-content clearfix">
			<div class="page-header">
				<p class="lead">Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec ullamcorper nulla non metus auctor fringilla. Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
				<div class="panel text-center">
					<img src="dist/images/medicos.png" class="img-responsive">
				</div>
				<p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec ullamcorper nulla non metus auctor fringilla. Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
			</div>
			<div class="alert alert-danger">
				Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec ullamcorper nulla non metus auctor fringilla.
			</div>
			<h4 class="form-title">Dados pessoais</h4>
			<form role="form">
				<div class="form-group has-error">
					<label for="name">Nome completo</label>
					<input type="text" class="form-control" id="name">
				</div>
				<div class="form-group row has-error">
					<div class="col-xs-12 col-sm-6">
						<label for="email">Email</label>
						<input type="email" class="form-control" id="email">
					</div>
					<div class="col-xs-12 col-sm-6">
						<label for="phone">Telefone</label>
						<input type="text" class="form-control" id="phone">
					</div>
				</div>
				<h4 class="form-title">Dados Candidatura</h4>
				<div class="form-group row has-error">
					<div class="col-xs-12 col-sm-6">
						<label for="subject">Área a que se candidata</label>
						<select class="selectpicker form-control" id="subject">
							<option>Pedido de informação</option>
							<option>Vulputate Sem Ligula</option>
							<option>Vulputate Sem Ligula</option>
							<option>Vulputate Sem Ligula</option>
						</select>
					</div>
					<div class="col-xs-12 col-sm-6">
						<label for="unidade">Unidade a que se candidata</label>
						<select class="selectpicker form-control" id="unidade">
							<option>Selecione uma unidade</option>
							<option>Vulputate Sem Ligula</option>
							<option>Vulputate Sem Ligula</option>
							<option>Vulputate Sem Ligula</option>
						</select>
					</div>
				</div>
				<div class="form-group has-error">
					<label for="habilitacoes">Habilitações</label>
					<input type="text" class="form-control" id="habilitacoes">
				</div>
				<div class="form-group has-error">
					<label for="message">Mensagem</label>
					<p class="help-block pull-right">Caracteres disponíveis: 250</p>
					<textarea id="message" class="form-control" rows="3"></textarea>
				</div>
				<div class="form-group add-file">
					<label for="cv">Curriculum Vitae</label>
					<label><small>Formato: .pdf; Máx.: 2MB</small></label>
					<input type="file" title="Adicionar CV +" class="file-inputs uppercase hidden-xs">
					<input type="file" title="Adicionar CV +" class="form-control uppercase visible-xs">
				</div>
				<div class="form-group pull-right">
					<a href="" class="link">Cancelar</a>
					<a href="conclusao.php" class="btn btn-secondary uppercase">Enviar candidatura</a>
				</div>
			</form>
		</div>

		<div class="col-md-4 sidebar">
			<!-- Side nav -->
			<ul class="nav nav-pills nav-stacked side-nav">
				<li><a href="">Quem Somos</a></li>
				<li><a href="">Missão e valores</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li class="active"><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
			</ul>
			
			<div class="panel">
				<img src="dist/images/hppEncontre.png" class="img-responsive">
			</div>
			<div class="panel">
				<img src="dist/images/hppMarcacoesSidebar.png" class="img-responsive">
			</div>

			<!-- Top posts -->
			<div class="panel panel-default top-posts">
				<div class="panel-heading">
					<h3 class="panel-title"><img src="dist/images/mais-vistos.png"> + Vistos</h3>
				</div>
				<div class="panel-body">
					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div><p >Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac.</p></div>
							</div>
						</div>
					</div>
					
					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div><p >Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac.</p></div>
							</div>
						</div>
					</div>

					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div><p >Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac.</p></div>
							</div>
						</div>
					</div>

					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div><p >Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac.</p></div>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>
	</div>
</div>


<?php include 'footer.php'; ?>