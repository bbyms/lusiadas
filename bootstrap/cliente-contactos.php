
<?php include 'header-login.php'; ?>

<div class="container main">


	<div class="row contact">

		<div class="col-md-8 main-content">
			<h1 class="page-title space-up">Contactos</h1>
			<p class="lead blue">Donec id elit non mi porta gravida at eget metus. Curabitur blandit tempus porttitor. Aenean lacinia bibendum nulla sed consectetur.</p>

			<div class="media">
				<a class="pull-left" href="#">
					<img class="media-object" src="dist/images/telefone.png" alt="...">
				</a>
				<div class="media-body">
					<span class="blue">Nº marcação único</span>
					<span class="number terciary">800 20 1000</span>
					<p>Grátis</p>
					<p>24h todos os dias</p>
				</div>
			</div>

			<div class="media">
				<a class="pull-left" href="#">
					<img class="media-object" src="dist/images/mail.png" alt="...">
				</a>
				<div class="media-body">
					<span class="blue">Email</span>
					<span class="number terciary">geral@lusiadas.pt</span>
					<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Maecenas faucibus mollis interdum.</p>
				</div>
			</div>

			<div class="media">
				<a class="pull-left" href="#">
					<img class="media-object" src="dist/images/person.png" alt="...">
				</a>
				<div class="media-body">
					<span class="blue">Presencialmente nas nossas unidades</span>
					<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Maecenas faucibus mollis interdum.</p>
				</div>
			</div>

		</div>

		<div class="col-md-4 sidebar">
			<div class="panel">
				<a href=""><img src="dist/images/consulta-ajuda.png" class="img-responsive"></a>
			</div>
			<div class="panel support">
				<span class="primary number">800 20 1000</span>
				<span><strong class="terciary">GRÁTIS</strong> <strong class="primary">24h</strong> <em class="primary">todos os dias</em></span>
    		</div>
    		<div class="panel contact-group">
    			<em>Email</em>
				<a class="primary" mailto:"geral@lusiadas.pt">geral@lusiadas.pt</a>
    		</div>
		</div>
	</div>
</div>


<?php include 'footer-login.php'; ?>