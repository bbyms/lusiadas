
<?php include 'header.php'; ?>

<div class="container main">
	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Lusiadas.pt</a></li>
				<li><a href="#">Sobre os Lusiadas</a></li>
				<li class="active"><a href="#">Trabalhe connosco</a></li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<h1 class="page-title">Trabalhe connosco</h1>
		</div>
	</div>

	<div class="row">

		<div class="col-md-8 main-content">
			<div class="panel conclusion">
				<h4 class="form-title">Candidatura enviada!</h4>
				<p class="lead">Vestibulum id ligula porta felis euismod semper.</p>
				<p>Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
			</div>
		</div>

		<div class="col-md-4 sidebar">
			<!-- Side nav -->
			<ul class="nav nav-pills nav-stacked side-nav">
				<li><a href="">Quem Somos</a></li>
				<li><a href="">Missão e valores</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li class="active"><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
				<li><a href="">Vehicula Vulputate</a></li>
			</ul>
			
			<div class="panel">
				<img src="dist/images/hppEncontre.png" class="img-responsive">
			</div>
			<div class="panel">
				<img src="dist/images/hppMarcacoesSidebar.png" class="img-responsive">
			</div>

			<!-- Top posts -->
			<div class="panel panel-default top-posts">
				<div class="panel-heading">
					<h3 class="panel-title"><img src="dist/images/mais-vistos.png"> + Vistos</h3>
				</div>
				<div class="panel-body">
					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div><p >Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac.</p></div>
							</div>
						</div>
					</div>
					
					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div><p >Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac.</p></div>
							</div>
						</div>
					</div>

					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div><p >Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac.</p></div>
							</div>
						</div>
					</div>

					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
						</a>
						<div class="media-body">
							<div class="ellipsis">
								<div><p >Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac.</p></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php include 'footer.php'; ?>