<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Lusíadas</title>
  <link href="dist/css/COREV15.css" rel="stylesheet" type="text/css" />
  <link href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
  <link href="dist/css/bootstrap.css" rel="stylesheet" type="text/css" />
  <link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700' rel='stylesheet' type='text/css'>
  <link rel="icon" type="image/png" href="dist/images/favicon.png" />

  <script src="dist/js/modernizr.js"></script>
  <script src="dist/js/css3-mediaqueries.js"></script>

  <meta http-equiv="X-UA-Compatible" content="IE=edge"> 

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>
    <body id="s4-bodyContainer"> 

      <!-- Top bar -->

      <div class="container top-links hidden-print">
        <div class="row">
          <div class="col-md-6 col-xs-6">
            <a href="" class="active idioma">PT</a> |
            <a href="" class="idioma">EN</a>
          </div>
          <div class="col-md-6 col-xs-6 top-nav text-right">
            <a href="cliente-contactos.php" class="hidden-xs">Fale connosco</a>
            <a href="" class="hidden-xs">Subscrever E-news</a>
          </div>
        </div>
      </div>


      <!-- header -->

      <div class="container main">

        <header class="row">
          <div class="col-md-7 col-sm-6 col-xs-5">
            <a href="index.php"><img src="dist/images/logoLusiadas.png" class="img-responsive"></a>
          </div>
        </header>

      </div>
