
<?php include 'header.php'; ?>

<div class="container main">
	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Lusiadas.pt</a></li>
				<li class="active"><a href="#">Hospitais e Clínicas</a></li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<h1 class="page-title">Hospitais e Clínicas</h1>
		</div>
	</div>

	<div class="row">

		<div class="col-md-8 main-content articles">
			<div class="page-header">
				<p class="space">Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec ullamcorper nulla non metus auctor fringilla. Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
			</div>
			
			<!-- Lista de conteúdos -->

			<div class="row row-xs-flex space-50">
				<!-- .cover-image para imagem ocupar 100% -->
				<div class="col-sm-6 col-xs-4 cover-image" style="background-image:url(dist/images/hpEspecialidades.jpg)">

				</div>
				<div class="col-sm-6 col-xs-8">
					<span href="" class="category">Porto</span>
					<h3><a href="">Hospital Lusíadas Porto</a></h3>
					<div class="ellipsis space">
						<div>
							<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
							<a href="hubconteudos-detail.php">Ler mais</a>
						</div>
					</div>
					<div class="clearfix">
						<div class="more primary pull-left">
							<a href="hubconteudos.php" class="btn btn-primary">Ver mais</a>
						</div>
						<div class="more blue pull-right">
							<a href="hubconteudos.php" class="btn btn-blue">Localização</a>
						</div>
					</div>
				</div>
			</div>

			<div class="row row-xs-flex space-50">
				<!-- .cover-image para imagem ocupar 100% -->
				<div class="col-sm-6 col-xs-4 cover-image" style="background-image:url(dist/images/hpEspecialidades.jpg)">

				</div>
				<div class="col-sm-6 col-xs-8">
					<span href="" class="category">Porto</span>
					<h3><a href="">Hospital Lusíadas Porto</a></h3>
					<div class="ellipsis space">
						<div>
							<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
							<a href="hubconteudos-detail.php">Ler mais</a>
						</div>
					</div>
					<div class="clearfix">
						<div class="more primary pull-left">
							<a href="hubconteudos.php" class="btn btn-primary">Ver mais</a>
						</div>
						<div class="more blue pull-right">
							<a href="hubconteudos.php" class="btn btn-blue">Localização</a>
						</div>
					</div>
				</div>
			</div>

			<div class="row row-xs-flex space-50">
				<!-- .cover-image para imagem ocupar 100% -->
				<div class="col-sm-6 col-xs-4 cover-image" style="background-image:url(dist/images/hpEspecialidades.jpg)">

				</div>
				<div class="col-sm-6 col-xs-8">
					<span href="" class="category">Porto</span>
					<h3><a href="">Hospital Lusíadas Porto</a></h3>
					<div class="ellipsis space">
						<div>
							<p>Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.<i></i></p>
							<a href="hubconteudos-detail.php">Ler mais</a>
						</div>
					</div>
					<div class="clearfix">
						<div class="more primary pull-left">
							<a href="hubconteudos.php" class="btn btn-primary">Ver mais</a>
						</div>
						<div class="more blue pull-right">
							<a href="hubconteudos.php" class="btn btn-blue">Localização</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-4 sidebar">
			<div class="panel contact-group">
				<h1 class="page-title">Contactos:</h1>
				<em>Telefone</em>
				<span class="terciary space">800 20 1000</span>
				<em>Email</em>
				<a class="terciary" mailto:"geral@lusiadas.pt">geral@lusiadas.pt</a>
			</div>
			<div class="panel">
				<img src="dist/images/hppEncontre.png" class="img-responsive">
			</div>
			<div class="panel">
				<img src="dist/images/hppMarcacoesSidebar.png" class="img-responsive">
			</div>
		</div>
	</div>
</div>


<?php include 'footer.php'; ?>