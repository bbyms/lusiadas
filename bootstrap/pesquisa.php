
<?php include 'header.php'; ?>

<div class="container main">
	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Lusiadas.pt</a></li>
				<li class="active"><a href="#">Pesquisa</a></li>
			</ol>
		</div>
	</div>


	<div class="row">
		<div class="col-md-8 main-content search">
			<h1 class="page-title">Pesquisa</h1>
			<div class="input-group space">
				<input type="search" value="ortopedia" class="form-control">
				<a href="" class="input-group-addon fa fa-search"></a>
			</div>
			<div class="panel pop">
				<h5 class="blue">Especialidades - <strong>Ortopedia</strong></h5>
				<p>Donec id elit non mi porta gravida at eget metus.</p>
				<a href="">lusiadas.pt/especialidades/<strong>ortopedia</strong>.aspx</a>
			</div>

			<div class="content hide">
				<h5 class="blue">Especialidades - <strong>Ortopedia</strong></h5>
				<p>Página web</p>
				<img src="dist/images/preview.jpg" class="img-responsive">
				<a href="" class="uppercase">view</a>
			</div>

			<div class="panel">
				<h5 class="blue">Especialidades - <strong>Ortopedia</strong></h5>
				<p>Curabitur blandit tempus porttitor. Etiam porta sem malesuada magna mollis euismod. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
				<a href="">lusiadas.pt/especialidades/<strong>ortopedia</strong>.aspx</a>
			</div>

			<div class="panel">
				<h5 class="blue">Especialidades - <strong>Ortopedia</strong></h5>
				<p>Donec id elit non mi porta gravida at eget metus.</p>
				<a href="">lusiadas.pt/especialidades/<strong>ortopedia</strong>.aspx</a>
			</div>

			<div class="panel">
				<h5 class="blue">Especialidades - <strong>Ortopedia</strong></h5>
				<p>Curabitur blandit tempus porttitor. Etiam porta sem malesuada magna mollis euismod. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
				<a href="">lusiadas.pt/especialidades/<strong>ortopedia</strong>.aspx</a>
			</div>

			<div class="panel">
				<h5 class="blue">Especialidades - <strong>Ortopedia</strong></h5>
				<p>Donec id elit non mi porta gravida at eget metus.</p>
				<a href="">lusiadas.pt/especialidades/<strong>ortopedia</strong>.aspx</a>
			</div>

			<div class="row">
				<div class="col-xs-12 text-center">
					<ul class="pagination">
						<li class="disabled arrow"><a href="#" class="previous"><span class="glyphicon glyphicon-arrow-left"></span> Página anterior</a></li>
						<li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
						<li><a href="#">2</a></li>
						<li class="arrow"><a href="#" class="next">Próxima página <span class="glyphicon glyphicon-arrow-right"></span></a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-4 sidebar">
			<a href="" class="btn btn-block btn-fade-1 uppercase">Hospitais e Clínicas</a>
			<a href="" class="btn btn-block btn-fade-2 uppercase">Médicos</a>
			<a href="" class="btn btn-block btn-fade-3 uppercase">Especialidades</a>
			<a href="" class="btn btn-block btn-fade-4 uppercase">Contactos</a>
			<a href="" class="btn btn-block btn-fade-5 uppercase">Fale connosco</a>
		</div>

	</div>
</div>


<?php include 'footer.php'; ?>