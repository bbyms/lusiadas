
<?php include 'header-unidades.php'; ?>

<div class="container main">
	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Hospital Lusíadas Lisboa</a></li>
				<li><a href="#">Apoio ao cliente</a></li>
				<li class="active"><a href="#">Perguntas frequentes</a></li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<h1 class="page-title">Perguntas Frequentes</h1>
		</div>
	</div>

	<div class="row">

		<div class="col-md-8 main-content">
			<p class="lead blue">Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec ullamcorper nulla non metus auctor fringilla. Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
			
			<!-- Accordion -->
			<div class="panel-group accordion" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="arrow-toggle collapsed">
								Acordos e Preçários <span class="pull-right"></span>
							</a>
						</h4>
					</div>
					<div id="collapseOne" class="panel-collapse collapse">
						<div class="panel-body">
							<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vestibulum id ligula porta felis euismod semper. Vestibulum id ligula porta felis euismod semper. Etiam porta sem malesuada magna mollis euismod. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Sed posuere consectetur est at lobortis. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="arrow-toggle collapsed">
								Faturas <span class="pull-right"></span>
							</a>
						</h4>
					</div>
					<div id="collapseTwo" class="panel-collapse collapse">
						<div class="panel-body">
							<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="arrow-toggle collapsed">
								Informação Geral <span class="pull-right"></span>
							</a> 
						</h4>
					</div>
					<div id="collapseThree" class="panel-collapse collapse">
						<div class="panel-body">
							<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading active">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="arrow-toggle">
								Médicos <span class="pull-right"></span>
							</a> 
						</h4>
					</div>
					<div id="collapseFour" class="panel-collapse collapse in">
						<div class="panel-body">
							<div class="panel-group sub-accordion" id="sub-accordion">
								<div class="panel">
									<div class="panel-heading">
										<a data-toggle="collapse" data-parent="#sub-accordion" href="#subcollapseOne">
											Collapsible Group Item #1
										</a>
									</div>
									<div id="subcollapseOne" class="panel-collapse collapse in">
										<div class="panel-body">
											<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
										</div>
									</div>
								</div>
								<div class="panel">
									<div class="panel-heading">
										<a data-toggle="collapse" data-parent="#sub-accordion" href="#subcollapseTwo">
											Collapsible Group Item #2
										</a>
									</div>
									<div id="subcollapseTwo" class="panel-collapse collapse">
										<div class="panel-body">
											<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
										</div>
									</div>
								</div>
								<div class="panel">
									<div class="panel-heading">
										<a data-toggle="collapse" data-parent="#sub-accordion" href="#subcollapseThree">
											Collapsible Group Item #3
										</a>
									</div>
									<div id="subcollapseThree" class="panel-collapse collapse">
										<div class="panel-body">
											<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" class="arrow-toggle collapsed">
								Consultas <span class="pull-right"></span>
							</a> 
						</h4>
					</div>
					<div id="collapseFive" class="panel-collapse collapse">
						<div class="panel-body">
							<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseSix" class="arrow-toggle collapsed">
								Exames <span class="pull-right"></span>
							</a> 
						</h4>
					</div>
					<div id="collapseSix" class="panel-collapse collapse">
						<div class="panel-body">
							<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-4 sidebar">
			<!-- Side nav -->
			<div class="panel-group accordion" id="unidades-nav">
				<div class="panel panel-default">
					<div class="panel-heading">
						<a href="unidades-marcacoes.php">
							Marcações
						</a>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<a href="">
							Visitas
						</a>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<a data-toggle="collapse" data-parent="#unidades-nav" href="#guia" class="arrow-toggle collapsed">
							Guia de acolhimento <span class="pull-right"></span>
						</a> 
					</div>
					<div id="guia" class="panel-collapse collapse">
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked side-nav">
								<li><a href="">Quem Somos</a></li>
								<li class="active"><a href="">Missão e valores</a></li>
								<li><a href="">Vehicula Vulputate</a></li>
								<li><a href="">Vehicula Vulputate</a></li>
								<li><a href="">Vehicula Vulputate</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<a data-toggle="collapse" data-parent="#unidades-nav" href="#precario" class="arrow-toggle collapsed">
							Preçário e Faturação <span class="pull-right"></span>
						</a> 
					</div>
					<div id="precario" class="panel-collapse collapse">
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked side-nav">
								<li><a href="">Quem Somos</a></li>
								<li class="active"><a href="">Missão e valores</a></li>
								<li><a href="">Vehicula Vulputate</a></li>
								<li><a href="">Vehicula Vulputate</a></li>
								<li><a href="">Vehicula Vulputate</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<a href="">
							Saúde de A-Z
						</a> 
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading active">
						<a href="unidades-faqs.php">
							Perguntas frequentes
						</a> 
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<a href="unidades-fale-connosco.php">
							Fale connosco
						</a> 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php include 'footer.php'; ?>