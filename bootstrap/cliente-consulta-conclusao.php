
<?php include 'header-clients.php'; ?>

<div class="container main">
	<div class="row">
		<div class="col-xs-12">
			<!-- Breadcrumb -->
			<ol class="breadcrumb">
				<li><a href="#">Portal Clientes Lusíadas</a></li>
				<li class="active"><a href="#">Marcar consulta</a></li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<h1 class="page-title blue">Marcar consulta</h1>
		</div>
	</div>

	<div class="row">

		<div class="col-md-8 space clearfix">

			<div class="panel conclusion">
				<h4 class="form-title">Marcação concluída com sucesso!</h4>
				<p class="lead">Vestibulum id ligula porta felis euismod semper <a href="" class="blue">aqui</a>.</p>
				<p>Maecenas sed diam eget risus varius blandit sit amet non magna?<br><a href="" class="blue">Peça a sua password aqui.</a></p>
			</div>

			<div class="media space">
				<a class="pull-left" href="#">
					<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
				</a>
				<div class="media-body">
					<label>Unidade</label>
					<big>Clínica Lusíadas Parque das Nações</big>
				</div>
			</div>

			<div class="panel">
				<label>Especialidade</label>
				<big>Cardiologia</big>
			</div>

			<div class="panel">
				<label>Tipo de consulta</label>
				<big>Consulta geral</big>
			</div>

			<div class="media space">
				<a class="pull-left" href="#">
					<img class="media-object" src="dist/images/noticia_thumbnail.jpg" alt="...">
				</a>
				<div class="media-body">
					<label>Médico</label>
					<big>Dr. Afonso Vasconcelos</big>
				</div>
			</div>

			<div class="panel">
				<label>Data e hora</label>
				<big>Quarta-feira, 9 de Abril às 11:00</big>
			</div>
		</div>

		<div class="col-md-4 sidebar">
			<div class="panel">
				<a href=""><img src="dist/images/consulta-ajuda.png" class="img-responsive"></a>
			</div>
			<div class="panel support">
				<span class="primary number">800 20 1000</span>
				<span><strong class="terciary">GRÁTIS</strong> <strong class="primary">24h</strong> <em class="primary">todos os dias</em></span>
    		</div>
    		<div class="panel contact-group">
    			<em>Email</em>
				<a class="primary" mailto:"geral@lusiadas.pt">geral@lusiadas.pt</a>
    		</div>
		</div>
	</div>
</div>


<?php include 'footer-clients.php'; ?>